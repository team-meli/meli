<!DOCTYPE html>
<html class="no-js" lang="en-US">
    <head>

       <title>4Carro</title>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="keywords" content="">

        <link rel="stylesheet" href="blum-assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="blum-assets/css/style.css">
        <link rel="stylesheet" href="blum-assets/css/utilities.css">

        <link rel="stylesheet" href="blum-assets/css/custom.css">

        <link rel="stylesheet" href="blum-assets/css/fontawesome/all.min.css" type="text/css">
        <link rel="stylesheet" href="blum-assets/css/themify-icons.css" type="text/css">
        <link rel="stylesheet" href="blum-assets/css/animate.min.css">
        <link rel="stylesheet" href="blum-assets/css/vegas.min.css">

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,300i,400,400i,700,700i%7CMontserrat:100,200,300,400,500,700">

        <link rel="shortcut icon" href="/assets/img/favicon.png" />
    <link rel="icon" type="image/x-icon" href="/assets/img/favicon.png">
    <link rel="icon" type="image/png" href="/assets/img/favicon.png" />

        <script src="blum-assets/js/modernizr-custom.js"></script>

    </head>
    <body>

        <div id="preloader" class="preloader">
            <div class="loader-status">
                <div class="spinner"></div>
            </div>
        </div>

        <!-- Global overlay -->
        <div class="global-overlay">
            <div class="overlay-inner bg-image-holder bg-cover">
                <img src="/blum-assets/images/background.jpg" alt="">
            </div>
            <!-- <div class="overlay-inner bg-primary opacity-85"></div> -->
        </div>

        <!-- Site header -->
        <header class="site-header header-mobile-dark header-content-light header-content-mobile-light">
            <div class="header-brand">
                <!-- Logo -->
                <a class="logo" href="#">
                    <!-- <img src="blum-assets/images/mialogo-w.png" alt=""> -->
                </a>
                <button type="button" id="navigation-toggle" class="nav-toggle">
                    <span></span>
                </button>
            </div>
            <div class="header-collapse">
                <div class="header-collapse-inner">
                    <nav class="site-nav">
                        @if(!empty($firstPart))
                        <ul id="navigation" style="" firstPart="{{ $firstPart }}">
                            @else
                        <ul id="navigation" style="" firstPart="home">
                            @endif
                            <li style="display: none;">
                                <a href="#home">Inicia Sesión</a>
                            </li>
                            <li style="display: none;">
                                <a  href="#signin">Inicia Sesión</a>
                            </li>
                            <li style="display: none;">
                                <a href="#signup">Regístrate</a>
                            </li>
                            <li style="display: none;">
                                <a href="#services">Services</a>
                            </li>
                            <li style="display: none;">
                                <a href="#contact">Contact</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>

        <div class="site-content">
            <div class="site-content-inner">

            

                <div class="site-part" id="home">
                    <section class="fullscreen d-flex">
                        <div class="container align-self-center text-white">
                            <div class="row text-center">
                                <div class="col-12 col-lg-9 mx-lg-auto">
                                    <h2 class="h1 mb-4 animated" data-animation="fadeInUp">
                                        Inicia Sesión
                                    </h2>
                                    <div class="divider animated" data-animation="fadeInUp" data-animation-delay="200"><span></span></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-lg-6 mx-lg-auto">
                                    <div class=" animated" data-animation="fadeInUp" data-animation-delay="600">
<!--             <form class="mb-0" id="sf" name="sf" method="POST" action="{{ route('login') }}" >
             @csrf
               <div class="form-row">


                   <div class="col-12 col-md-12">
                       <div class="form-group text-white">
                           <input type="email" id="sf-email" name="sf-email"  class="form-control required"> 

   @if ($errors->has('email'))
       <span class="invalid-feedback" role="alert">
           <strong>{{ $errors->first('email') }}</strong>
       </span>
   @endif

                       </div>
                   </div>
                   <div class="col-12 col-md-12">
                       <div class="form-group text-white">
                           <input type="password" id="sf-email" name="sf-email" placeholder="Escribe tu contraseña aquí" class="form-control required">
                       </div>
                   </div>

                   <div class="col-12 col-md-3">
                       <button class="btn btn-dark btn-block" type="submit" id="sf-submit" name="sf-submit">Ingresar</button>
                   </div>

               </div>
           </form> -->


        <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="py-3">
                <div class="form-group">
                
                <input id="email" type="email" class="form-control form-control-alt form-control-lg form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Escribe tu Email">

        @if ($errors->has('email'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif

                </div>
                <div class="form-group">
                    <input type="password" class="form-control form-control-alt form-control-lg form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" id="login-password" name="password" placeholder="Ingresa tu contraseña" required>
                    @if ($errors->has('password'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
                </div>
<!--                 <div class="form-group">
                    <div class="custom-control custom-checkbox">
                         <input class="form-check-input custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label class="custom-control-label font-w400" for="login-remember">Recuerda mis datos</label>
                    </div>
                </div> -->
            </div>
            <div class="form-group row">
                <div class="col-12 col-md-3">
                       <button class="btn btn-dark btn-block" type="submit" id="sf-submit" name="sf-submit">Ingresar</button>
                   </div>
            </div>
        </form>

                                        <div class="subscribe-form-result pt-2"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <div class="site-part" id="signup">
                    <section class="fullscreen d-flex">
                        <div class="container align-self-center text-white">
                            <div class="row">
                                <div class="col-12 col-lg-10 mx-lg-auto text-center">
                                    <h2 class="h1 mb-4 animated" data-animation="fadeInUp">Regístrate</h2>
                                    <div class="divider animated" data-animation="fadeInUp" data-animation-delay="200"><span></span></div>
                                    <div class="animated" data-animation="fadeInUp" data-animation-delay="400">

        <form method="POST" action="{{ route('register') }}">
            @csrf
            <div class="py-3">
              <div class="form-group row">
                <div class="col-12 col-md-6">
                <input id="name" type="name" class="form-control form-control-alt form-control-lg form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus placeholder="Nombre completo">
                    @if ($errors->has('name'))
                       <span class="invalid-feedback" role="alert" style="color: white">
                           <strong>{{ $errors->first('name') }}</strong>
                       </span>
                    @endif
                </div>
                <div class="col-12 col-md-6">
                   <input id="email" type="email" class="form-control form-control-alt form-control-lg form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Escribe tu Email">
                   @if ($errors->has('email'))
                       <span class="invalid-feedback" role="alert" style="color: white">
                           <strong>{{ $errors->first('email') }}</strong>
                       </span>
                   @endif
                </div>
              </div>
              <div class="form-group row">
                <div class="form-group col-12 col-md-6">
                    <input type="password" class="form-control form-control-alt form-control-lg form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" id="login-password" name="password" placeholder="Ingresa tu contraseña" required>
                    @if ($errors->has('password'))
                      <span class="invalid-feedback" role="alert" style="color: white">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                    @endif
                </div>
                <div class="form-group col-12 col-md-6">
                    <input type="password" class="form-control form-control-alt form-control-lg form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" id="password_confirmation" name="password_confirmation" placeholder="Confirma tu contraseña" required>
                    @if ($errors->has('password_confirmation'))
                      <span class="invalid-feedback" role="alert" style="color: white">
                          <strong>{{ $errors->first('password_confirmation') }}</strong>
                      </span>
                    @endif
                </div>
              </div>


            </div>
            <div class="form-group row" style="text-align: left !important;">

                <div class="col-12 col-md-3">
                       <button class="btn btn-dark btn-block" type="submit" id="sf-submit" name="sf-submit">Regístrate</button>
                </div>
            </div>
        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                <div class="site-part" id="services">
                    <section class="fullscreen d-flex">
                        <div class="container align-self-center text-white">
                            <div class="row mb-5 text-center">
                                <div class="col-12 col-lg-9 mx-lg-auto">
                                    <h2 class="h1 mb-4 animated" data-animation="fadeInUp">Services</h2>
                                    <div class="divider animated" data-animation="fadeInUp" data-animation-delay="200"><span></span></div>
                                    <p class="animated" data-animation="fadeInUp" data-animation-delay="400">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-12 col-lg-4 mb-5 mb-lg-0">
                                    <div class="feature-block animated" data-animation="fadeInUp" data-animation-delay="600">
                                        <div class="feature-icon mb-3">
                                            <div>
                                                <i class="ti-timer"></i>
                                            </div>
                                        </div>
                                        <h3 class="h4 mb-2">Launch Quickly</h3>
                                        <p>Quisque ultrices non velit sit amet consectetur. Cras turpis dolor, facilisis a nibh non, ullamcorper facilisis mauris. Nulla rutrum arcu sed ligula malesuada, quis condimentum eros sollicitudin.</p>
                                    </div>
                                </div>

                                <div class="col-12 col-lg-4 mb-5 mb-lg-0">
                                    <div class="feature-block animated" data-animation="fadeInUp" data-animation-delay="800">
                                        <div class="feature-icon mb-3">
                                            <div>
                                                <i class="ti-brush-alt"></i>
                                            </div>
                                        </div>
                                        <h3 class="h4 mb-2">Stylish Design</h3>
                                        <p>Quisque ultrices non velit sit amet consectetur. Cras turpis dolor, facilisis a nibh non, ullamcorper facilisis mauris. Nulla rutrum arcu sed ligula malesuada, quis condimentum eros sollicitudin.</p>
                                    </div>
                                </div>

                                <div class="col-12 col-lg-4">
                                    <div class="feature-block animated" data-animation="fadeInUp" data-animation-delay="1000">
                                        <div class="feature-icon mb-3">
                                            <div>
                                                <i class="ti-book"></i>
                                            </div>
                                        </div>
                                        <h3 class="h4 mb-2">Well Documented</h3>
                                        <p>Quisque ultrices non velit sit amet consectetur. Cras turpis dolor, facilisis a nibh non, ullamcorper facilisis mauris. Nulla rutrum arcu sed ligula malesuada, quis condimentum eros sollicitudin.</p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </section>
                </div>

                <div class="site-part" id="contact">
                    <section class="fullscreen d-flex">
                        <div class="container align-self-center text-white">
                            <div class="row text-center">
                                <div class="col-12 col-lg-9 mx-lg-auto">
                                    <h2 class="h1 mb-4 animated" data-animation="fadeInUp">Contact Us</h2>
                                    <div class="divider animated" data-animation="fadeInUp" data-animation-delay="200"><span></span></div>
                                    <p class="animated mb-5" data-animation="fadeInUp" data-animation-delay="400">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                    <div class="contact-form text-left animated" data-animation="fadeInUp" data-animation-delay="600">
                                        <form class="mb-0" id="cf" name="cf" action="include/sendemail.php" method="post" autocomplete="off">
                                            <div class="form-row">

                                                <div class="form-process"></div>

                                                <div class="col-12 col-md-6">
                                                    <div class="form-group error-text-white">
                                                        <input type="text" id="cf-name" name="cf-name" placeholder="Enter your name" class="form-control required">
                                                    </div>
                                                </div>

                                                <div class="col-12 col-md-6">
                                                    <div class="form-group error-text-white">
                                                        <input type="email" id="cf-email" name="cf-email" placeholder="Enter your email address" class="form-control required">
                                                    </div>
                                                </div>

                                                <div class="col-12">
                                                    <div class="form-group error-text-white">
                                                        <input type="text" id="cf-subject" name="cf-subject" placeholder="Subject (Optional)" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-12 mb-4">
                                                    <div class="form-group error-text-white">
                                                        <textarea name="cf-message" id="cf-message" placeholder="Here goes your message" class="form-control required" rows="7"></textarea>
                                                    </div>
                                                </div>

                                                <div class="col-12 d-none">
                                                    <input type="text" id="cf-botcheck" name="cf-botcheck" value="" class="form-control" />
                                                </div>

                                                <div class="col-12 text-center">
                                                    <button class="btn btn-dark" type="submit" id="cf-submit" name="cf-submit">Send Message</button>
                                                </div>

                                            </div>
                                        </form>
                                        <div class="contact-form-result text-center"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

            </div>
        </div>

        <!-- Site footer -->
        <footer class="site-footer footer-content-light footer-mobile-content-light">
            <div class="overlay">
                <div class="overlay-inner bg-white"></div>
            </div>
            <div class="container socials-container">
                <nav class="socials-icons">
                    <ul class="mx-auto">
<!--                         <li><a target="_blank" href="https://www.facebook.com/miascootersco/"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a target="_blank" href="https://www.instagram.com/miascootersco/"><i class="fab fa-instagram"></i></a></li> -->
                    </ul>
                </nav>
            </div>
            <div class="container copyright-container">
                <p>© 2019 4Carro</p>
            </div>
        </footer>

        <script src="blum-assets/js/jquery.min.js"></script>
        <script src="blum-assets/js/jquery.easing.min.js"></script>
        <script src="blum-assets/js/jquery.validate.min.js"></script>
        <script src="blum-assets/js/jquery.form.min.js"></script>
        <script src="blum-assets/js/jquery.countdown.min.js"></script>
        <script src="blum-assets/js/granim.min.js"></script>
        <script src="blum-assets/js/vegas.min.js"></script>
        <script src="blum-assets/js/jquery.mb.YTPlayer.min.js"></script>

        <script src="blum-assets/js/main.js"></script>

    </body>
</html>