@extends('layouts.backend')

@section('content')
    <link rel="stylesheet" type="text/css" href="/css/custom-mia-dataTable.css">
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                    {{$Modelname}} <br>
                    Se han taggeado {{$ModelTag}} de {{$ModelCant}} 
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">

                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

     <!-- Page Content -->
    <div class="content">
        <!-- Dynamic Table Full -->
        <div class="block">
            @if(isset($Versions))
                <div class="block-content block-content-full">
                    @if (session('message'))
                        <div class="alert alert-success">
                            {{ session('message') }}
                        </div>
                    @endif 
                    <table class="table table-bordered table-striped table-vcenter init-dataTable">
                        <thead>
                            <tr>
                                <th filter-type='text'> <span class="title-head" >Modelo</span> </th>
                                
                                <th filter-type='multi_select'> <span class="title-head" >Version</span> </th>
                                <th> <span class="title-head" >Cant. Taggeados</span> </th>
                                <th> <span class="title-head" >Acciones</span> </th> 
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($Versions as $Version)
                            <tr>
                                <td>{{ $Version->modelo }}</td>
                                <td class="text-center font-size-sm">{{ $Version->name }}</td>
                                <td class="text-center font-size-sm">{{ $Version['taggeados'] }}</td>
                                <td class="d-none d-sm-table-cell">
                                    <a href="/dash/publication/byversion/{{$Version->id}}"  class="btn btn-sm btn-outline-primary"  target="_blank">Publicaciones</a> 
                                    <a href="/dash/versions/{{$Version->id}}/edit"  class="btn btn-sm btn-outline-success" target="_blank">Taggear</a>                            
                                    <form action="/dash/versions/{{$Version->id}}" method="POST" style="    display: inline-block;">
                                        @method('DELETE')
                                        {{ csrf_field() }}
                                        <a  href="" class="delete_button btn btn-sm btn-outline-danger">Eliminar</a>
                                    </form>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td class="text-center font-size-sm">#</td>
                                <td class="font-w600 font-size-sm">
                                    <a>No hay versiones registradas</a>
                                </td>
                                <td></td> 
                            </tr>
                            @endforelse 
                        </tbody>
                    </table>
                </div>
            @endif

            @if(isset($Brands))
                <div class="block-content block-content-full">
                    @if (session('message'))
                        <div class="alert alert-success">
                            {{ session('message') }}
                        </div>
                    @endif 
                    <table class="table table-bordered table-striped table-vcenter init-dataTable">
                        <thead>
                            <tr> 
                                <th filter-type='text'> <span class="title-head" >Nombre</span> </th>
                                <th filter-type='multi_select'> <span class="title-head" >Cantidad Versiones</span> </th>
                                <th filter-type='multi_select'> <span class="title-head" >Cantidad Pub</span> </th>
                                <th> <span class="title-head" >Link</span> </th> 
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($Brands as $Brand)
                            <tr>
                                <td>{{ $Brand['name'] }}</td>
                                <td class="text-center font-size-sm">{{ $Brand['cantVersions'] }}</td> 
                                <td class="text-center font-size-sm">{{ $Brand['cantPubs'] }}</td> 
                                <td class="d-none d-sm-table-cell">
                                    <a href="/dash/version/view/{{$Brand['id']}}"  class="btn btn-sm btn-outline-success">Ver</a> 
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td class="text-center font-size-sm">#</td>
                                <td class="font-w600 font-size-sm">
                                    <a>No hay versiones registradas</a>
                                </td>
                                <td></td> 
                                <td></td> 
                            </tr>
                            @endforelse 
                        </tbody>
                    </table>
                </div>
            @endif
        </div>
        <!-- END Dynamic Table Full -->
    </div>
    <!-- END Page Content --> 
@endsection
@section('js_after')
    <script src="/js/custom-mia-dataTable.js"></script>  
    <script type="text/javascript">
        $(document).ready(function () {
            'use strict';  
            var oTables = smartInitDateTable(); 
        });
    </script> 
@endsection