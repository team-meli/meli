@extends('layouts.backend')

@section('content')
    <link rel="stylesheet" type="text/css" href="/css/custom-mia-dataTable.css">
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">Version</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">
                            <a href="/dash/versions" class="link-fx btn btn-sm btn-outline-primary">Todas las versiones</a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx btn btn-sm btn-primary" href="/dash/versions/create">Registrar nueva version</a>
                        </li>
                    </ol>
                </nav>
            </div>
       </div>
    </div>
    <!-- END Hero -->

     <!-- Page Content -->
    <div class="content"> 
        <!-- Alternative Style -->
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Registro de version</h3>
            </div>
            @if (session('info'))
                <div class="alert alert-success">
                    {{ session('info') }}
                </div>
            @endif
            <div class="block-content block-content-full">
                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif 
                <form id="thisversion" action="/dash/versions/{{$Version->id}}" method="post" validate=true version_id="{{$Version->id}}">
                    @method('PUT')
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-lg-4">
                            <p class="font-size-sm text-muted">
                                Una vez seleccionas el modelo, debes indicar las versiones relacionadas en el modulo inferior. 
                            </p>
                        </div>
                        <div class="col-lg-8 col-xl-5"> 
                            <div class="form-group">
                                <label for="name">Marca</label>
                                <input disabled id='brand_name' class="form-control form-control d-block" name="brand_name">
                                <input class="hide" id="brand_code" name="brand_code">
                            </div>

                            <div class="form-group">
                                <label for="name">Modelo que corresponde a la version</label> 
                                <input disabled id='model_name' class="form-control form-control">
                                <input class="hide" id="model" name="model_name">
                            </div>

                            <div class="form-group">
                                <label for="name">Nombre de la version</label>
                                <input type="text" id='name' class="form-control form-control d-block" id="example-text-input-alt" name="name" placeholder="Nombre de version" value="{{$Version->name }}" >
                            </div>

                            <button type="success" class="btn btn-primary">Editar Version</button> <br>
                            <small><b>Importante:</b> Si guardas la versión con una marca y modelo distinto, se eliminarán las subversiones relacionadas actuales.</small>
                            <hr>
                            <small>Este botón relacionará las publicaciones guardadas en la base de datos con la versión que estás editando, activa esta opción <b>después</b> de indicar todas las subversiones:</small>
                            <br><br>
                            <a href="publications" class="btn btn-success">Taggear Publicaciones y Crear Promedios</a>
                            <div id="msg"></div> <br>
                            <a href="/dash/publications/more/attr/{{$Version->model_id}}/{{$Version->id}}" class="btn btn-success mt-3">Traer mas atributos</a>
                        </div>
                    </div>
                </form> 
            </div>
        </div>
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Selecciona las Versiones que están dentro de esta versión</h3>
            </div>
                @if (session('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                @endif
 
                <!-- Bloque de opciones --> 
                <div class="block-options-dataTable">
                    <div class="tittle-option">Acciones en lote</div>
                    <div class="buttons-option">
                        <button id="check-all" class="btn btn-sm btn-outline-danger">
                            Marcar todas
                        </button>        
                        <button id="uncheck-all" class="btn btn-sm btn-outline-danger">
                            Desmarcar todas
                        </button>
                        <button id="toggle-all" class="btn btn-sm btn-outline-danger">
                            Toggle
                        </button>                
                    </div>
                </div> 

                <table id="subversiones" class="table table-bordered table-striped table-vcenter init-dataTable">
                    <thead>
                        <tr>
                            <th><span>Link ML</span></th>
                            <th filter-type ='text'> <span class="title-head">Subversion</span> </th>
                            <th filter-type ='multi_select'> <span class="title-head">Puertas</span> </th>
                            <th filter-type ='multi_select'> <span class="title-head">Transmisión</span> </th>  
                            <th filter-type ='multi_select'> <span class="title-head">Cilindraje</span> </th>  
                            <th filter-type ='multi_select'> <span class="title-head">Combustible</span> </th>  
                            <th filter-type ='multi_select'> <span class="title-head">Aire</span> </th>  
                            <th><span>Acciones</span></th>
                        </tr>
                    </thead> 
                    <tbody>
                        <tr>
                            <td class="font-w600 font-size-sm">
                                <a >Cargando</a>
                            </td>
                            <td></td> 
                            <td></td> 
                            <td></td> 
                            <td></td> 
                            <td></td>
                            <td></td> 
                            <td></td> 
                        </tr> 
                    </tbody>
                </table>
        </div> 
        <!-- END Alternative Style --> 
    </div>
    <!-- END Page Content --> 
    <style type="text/css">
        table.display {
            table-layout: fixed !important;          
        }
    </style>
@endsection
@section('js_after') 
    <script src="/js/custom-mia-dataTable.js"></script>   
    <script type="text/javascript"> 
    $(document).ready(function () {
        var oTables = smartInitDateTable();  

        var model_id = "{!! $Version->model_id !!}"; 
        var brand_id = "{!! $Version->brand_id !!}";
        var model_name = getName(model_id);
        $('#model_code').val(model_id);
        $('#brand_code').val(brand_id);
        $('#brand_name').val(getName(brand_id));
        $('#model_name').val(model_name);
        $('#model').val(model_name); 
        var version_id = $('#thisversion').attr("version_id");

        getSubVersions(version_id,model_id);

        $("#brand").change(function(){
            $('#modelo').html('<option value="0">Cargando</option>');
            var brand = $(this).children("option:selected").val();
            $.ajaxSetup({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
            });
            $.ajax({
                url: "/dash/models/brand/"+brand+"",
                method: 'GET',
                data: {
                    brand: brand,
                },
                success: function(result){
                    console.log(result);
                    $('#modelo').html('<option value="0">Selecciona un modelo</option>');
                    var models = JSON.parse(result);
                    $.each( models, function( key, value ) {
                        $('#modelo').append(new Option(value, value));
                    });
                },
                error:function (error) {
                	console.log(error);
                }
            }); 
        });

 		$(document).on('click','#getAllPubb',function(e){
 			e.preventDefault();
 			$(this).html('<i class="fa fa-fw fa-circle-notch fa-spin"></i>Consultando');
 			var response = getAllPub(brand_id,model_id); 
 			console.log(response);
	        if (!response['status']) {
	            $(this).html('Crear Versión').attr('disabled',false);
	            $('#msg').html('<div class="col-12 alert alert-danger animated fadeIn">'+
	                '<span class="fa fa-fw fa-info-circle"></span>'+
	                '<span class="ml-2">No se encontro publicaciones para este modelo en Mercado Libre</span>'+
	                '</div>');
	        }else{ 
	        	location.reload(); 
	        }
 		});

        $('#check-all').click(function(){
            $('input[name="example-cb-custom-primary-lg1"]').each(function(){ 
                if(!$(this).is(':checked')){ $(this).click(); } 
            });
        });

        $('#uncheck-all').click(function(){
            $('input[name="example-cb-custom-primary-lg1"]').each(function(){
                if($(this).is(':checked')){ $(this).click(); } 
            });
        });

        $('#toggle-all').click(function(){
            $('input[name="example-cb-custom-primary-lg1"]').each(function(){
                $(this).click();
            });
        });

        function getSubVersions(version_id,model){
            $.ajaxSetup({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
            });
            $.ajax({
                url: "/dash/versions/"+version_id+"/model/"+model+"/subversions",
                method: 'GET',
                data: {
                    model: model,
                },
                success: function(data){  
                    var data = JSON.parse(data);   
                    var rows=[];
                    var cont=0;
                    $.each(data,function(key,row) {
                        if (row.checked == '') {
                            var sort = '<span class="hide">0</span>';
                        }else{
                            var sort = '<span class="hide">1</span>';
                        }
                        rows.push([
                            "<a href="+row.link+" target='_blank' class='btn btn-success'>Link</a>",
                            "<span>"+row.realversion+"</span>",
                            "<span>"+row.doors+"</span>",
                            "<span>"+row.transmission+"</span>",
                            "<span>"+row.cilindraje+"</span>",
                            "<span>"+row.combustible+"</span>",
                            "<span>"+row.aire+"</span>",
                            sort+"<div class='custom-control custom-checkbox custom-control-primary custom-control-lg mb-1'>"+
                                "<input type='checkbox' class='custom-control-input' id='checkversion"+cont+"' version='"+row.realversion+"' doors='"+row.doors+"' transmission='"+row.transmission+"' "+row.checked+" name='example-cb-custom-primary-lg1'>"+
                                "<label class='custom-control-label' for='checkversion"+cont+"'></label>"+
                            "</div>",
                        ]); 
                        cont++;
                    }); 
                    oTables[0].clear().draw();
                    oTables[0].rows.add(rows).draw(); 

                    if (data.length<1) {
                    	$('#thisversion>div a[href="publications"]').remove();
                    	$('#thisversion div.col-lg-8').append('<button id="getAllPubb" class="btn btn-success">Traer Publicaciones</a>');
                    }

                    // $.each( models, function( key, value ) {
                    //     cont++;
                    //     var doors = value.doors;
                    //     if(doors=='null' || doors==null){
                    //         doors = 0;
                    //     }



                    //     $("#subversiones tbody").append(
                    //         "<tr>" +
                    //             "<td class='text-center font-size-sm'>"+
                    //                 "<a href="+value.link+" target='_blank' class='btn btn-success'>Link</a>"+
                    //             "</td>"+
                    //             "<td class='text-center font-size-sm'>"+value.realversion+"</td>"+
                    //             "<td class='text-center font-size-sm'>"+doors+"</td>"+
                    //             "<td class='text-center font-size-sm'>"+value.transmission+"</td>"+
                    //             "<td class='text-center font-size-sm'>"+value.cilindraje+"</td>"+
                    //             "<td class='d-none d-sm-table-cell'>"+
                    //                 "<div class='custom-control custom-switch custom-control-success custom-control-lg mb-2'>"+
                    //                     "<input type='checkbox' class='custom-control-input' id='checkversion"+cont+"' version='"+value.realversion+"' doors='"+value.doors+"' transmission='"+value.transmission+"' "+value.checked+" name='example-sw-success-lg2'>"+
                    //                     "<label class='custom-control-label' for='checkversion"+cont+"'></label>"+
                    //                 "</div>"+
                    //             "</td>" +
                    //         "</tr>"
                    //     );
                    // });
                    // onChangeChecks();
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
 
        $(document).on('change','input[type=checkbox]', function () {  
            var thisthis = $(this);  
            if($(this).is(':checked')){
                check_tag(thisthis);
            }else{
                uncheck_tag(thisthis);
            }
        }); 

        function check_tag(thisthis) {
            var SubVersionName = $(thisthis).attr('version');
            var SubVersionTransmission = $(thisthis).attr('transmission');
            var SubVersionDoors = $(thisthis).attr('doors');
            var version_id = $('#thisversion').attr("version_id");
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: "/dash/versions/subversion/create",
                data:{ 
                    version_id:version_id, 
                    name:SubVersionName, 
                    transmission:SubVersionTransmission, 
                    doors:SubVersionDoors
                },
                method: 'POST',
                success: function(result){ 
                    // thisthis.parent().find('label').html('Actualizado');
                    // thisthis.attr('checked',true);
                },
                error: function (e) {
                    console.log(e);
                }
            });
        }

        function uncheck_tag(thisthis) {
            var SubVersionName = $(thisthis).attr('version');
            var SubVersionTransmission = $(thisthis).attr('transmission');
            var SubVersionDoors = $(thisthis).attr('doors');
            var version_id = $('#thisversion').attr("version_id");
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: "/dash/versions/"+version_id+"/subversion/"+SubVersionName+"/delete",
                method: 'GET',
                success: function(result){ 
                    // thisthis.parent().find('label').html('Eliminado');
                    // thisthis.attr('checked',false);
                },
                error:function(e) {
                    console.log(e);
                }
            });
        }
 
    });
    </script> 
@endsection