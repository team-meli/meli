@extends('layouts.backend')

@section('content')
    <link rel="stylesheet" type="text/css" href="/css/custom-mia-dataTable.css">
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">Versiones</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">
                            <a href="/dash/versions/" class="link-fx btn btn-sm btn-outline-primary">Todas las versiones</a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx btn btn-sm btn-primary" href="/dash/versions/create">Registrar una version</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero --> 

    <!-- Page Content -->
    <div class="content"> 
        <!-- Alternative Style -->
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Registro de Version</h3>
            </div>
            <div class="block-content block-content-full">
                @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
                @endif 

                <form action="/dash/versions" method="POST" validate=true>
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-lg-4">
                            <p class="font-size-sm text-muted">
                                Una vez creada la versión, tendrás que indicar la relación entre las versiones del sistema. 
                            </p>
                        </div>
                        <div class="col-lg-8 col-xl-5">
                            <div class="form-group">
                                <label for="name">Marca</label>
                                <select name="brand" id="brand" class="js-select2 form-control">
                                    <option value="0">Selecciona una marca</option> 
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="name">Modelo</label>
                                <select disabled name="model_code" id="model" class="js-select2 form-control">
                                    <option value="0">Selecciona una marca primero</option>
                                </select>
                                <input class="hide" name="model_name">
                            </div>
                            <div class="form-group">
                                <label for="name">Nombre de la version</label>
                                <input disabled type="text" id='name' class="form-control form-control-alt" id="example-text-input-alt" name="name" placeholder="Nombre de version" value="{{ old('name') }}">
                            </div> 
                            <div id="msg"></div>
                            <button type="success" class="btn btn-primary next">Crear Versión</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- END Alternative Style -->
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Versiones Sugeridas para este modelo</h3>
            </div>
            @if (session('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
            @endif
            <div class="block-content block-content-full"> 
                <!-- Bloque de opciones --> 
                <div class="block-options-dataTable">
                    <div class="tittle-option">Vistas</div>
                    <div class="buttons-option">
                        <button href="#" class="btn btn-sm btn-outline-danger"  
                            custom-filter='[Retraso|moroso]' 
                            custom-filter-column='7' 
                            custom-sort='asc' 
                            custom-sort-column='6'>
                            Cartera Vencida
                        </button>                  
                    </div>
                </div> 

                <table class="table table-bordered table-striped table-vcenter init-dataTable">
                    <thead>
                        <tr>
                            <th><span class="title-head" > id </span></th>
                            <th><span class="title-head" > Nombre </span></th>
                            <th><span class="title-head" > Modelo </span></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="font-w600 font-size-sm">
                                <a >Elige una marca y un modelo para cargar las versiones sugeridas</a>
                            </td>
                            <td></td>
                            <td></td> 
                        </tr> 
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- END Page Content --> 
@endsection

@section('js_after')
    <script src="/js/custom-mia-dataTable.js"></script>  
    <script type="text/javascript"> 
        $(function(){   
            var brand;
            var model_code;
            getBrands(); 

            var oTables = smartInitDateTable();  
            var has_pub= false;

            // step 1
            $("#brand").change(function(){
                $('#model').html('<option value="0">Cargando</option>'); 
                brand = $(this).children("option:selected").val();
                getModels(brand); 
            });  
            
            // step 2
            $("#model").change(function(){ 
                $('#name').attr('disabled',false); 
                model_code = $(this).children("option:selected").val();
                model_name = $(this).children("option:selected").attr('ml-name');
                $('input[name="model_name"]').val(model_name);
                has_pub= hasPub(model_code);  
                if (has_pub) { 
                    $('#msg').html(''); 
                    fillTabla(model_code);
                }else{ 
                    $('#msg').html('<div class="col-12 alert alert-info animated fadeIn">'+
                        '<span class="fa fa-fw fa-info-circle"></span>'+
                        '<span class="ml-2">No hay publicaciones de este modelo, antes de crear la version se traeran publicaciones de Mercado Libre</span>'+
                    '</div>');
                    oTables[0].clear().draw();
                } 
            });  

            // step 3
            $('#name').keyup(function(key) {
                var length = $('#name').val().length;
                // $('.next').attr('disabled',(length>=3)? false:true);
            });

            // step 4
            $('.next').click(function(e) {
                e.preventDefault(); 
                $(this).html('<i class="fa fa-fw fa-circle-notch fa-spin"></i> Creando').attr('disabled',true);
                if(!has_pub){
                    var response = getAllPub(brand,model_code); 
                    if (!response['status']) {
                        $(this).html('Crear Versión').attr('disabled',false);
                        $('#msg').html('<div class="col-12 alert alert-danger animated fadeIn">'+
                            '<span class="fa fa-fw fa-info-circle"></span>'+
                            '<span class="ml-2">No se encontro publicaciones para este modelo en Mercado Libre</span>'+
                            '</div>');
                    }else{
                        sendForm();
                    }
                }else{
                    sendForm();
                }
            });

            function sendForm(){
                $('#msg').html('');
                $('form').submit();
            }

            function fillTabla(code_model) {
                $.ajaxSetup({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
                });
                $.ajax({ 
                    url: "/dash/helper/get_versions/"+model_code,
                    method: 'GET',  
                    success: function (data) {   
                        var rows=[];
                        $.each(data['data'],function(key,row) {
                            rows.push([
                                row.id,
                                row.name,
                                row.modelo
                            ]);
                        }); 
                        oTables[0].clear().draw();
                        oTables[0].rows.add(rows).draw(); 
                    }
                }); 
            }
            
            function getSubVersions(version_id,model){
                $.ajaxSetup({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
                });
                $.ajax({
                    url: "/dash/versions/model/"+model+"/subversions/hiddingtakens",
                    method: 'GET',
                    data: {
                        model: model,
                    },
                    success: function(result){
                        console.log(result);
                        $("#subversiones tbody").html('');
                        var models = JSON.parse(result);
                        cont=0;
                        $.each( models, function( key, value ) {
                            cont++;
                            var doors = value.doors;
                            if(doors=='null' || doors==null){
                                doors = 0;
                            }
                            $("#subversiones tbody").append(
                              "<tr>" +
                              "<td class='text-center font-size-sm'>"+value.realversion+" </td><td class='text-center font-size-sm'>"+doors+"</td> <td class='text-center font-size-sm'>"+value.transmission+"</td>" +
                              "" +
                              "</tr>"
                              );
                        });
                    }
                });
            } 
        });
    </script> 
@endsection
