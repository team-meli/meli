@extends('layouts.backend')

@section('content')
<link rel="stylesheet" type="text/css" href="/css/custom-mia-dataTable.css">
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">Promedios guardados</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">
                            <a href="/dash/averages" class="link-fx btn btn-sm btn-outline-primary">Todas los promedios</a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx btn btn-sm btn-primary" href="/dash/averages/create">Realizar nuevo promedio</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <!-- Dynamic Table Full -->
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Promedios <small></small></h3>
            </div>
            <div class="block-content block-content-full">
                <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                @if (session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
                @endif
                <!-- Bloque de opciones --> 
                <div class="block-options-dataTable">
                    <div class="tittle-option">Vistas</div>
                    <div class="buttons-option">
                        <button href="#" class="btn btn-sm btn-outline-danger"  
                            custom-filter='[Retraso|moroso]' 
                            custom-filter-column='7' 
                            custom-sort='asc' 
                            custom-sort-column='6'>
                            Cartera Vencida
                        </button>                  
                    </div>
                </div> 

                <table class="table table-bordered table-striped table-vcenter init-dataTable">
                    <thead>
                        <tr>
                            <th filter-type ='text'><span class="title-head">ID</span></th>
                            <th filter-type ='text'><span class="title-head">Promedio ml</span></th>
                            <th filter-type ='text'><span class="title-head">Promedio motor</span></th>
                            <th filter-type ='text'><span class="title-head">Modelo</span></th>
                            <th filter-type ='text'><span class="title-head">Año</span></th>
                            <th filter-type ='text'><span class="title-head">Version</span></th>
                            <th filter-type ='text'><span class="title-head">Acciones</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($Averages as $Average)
                        <tr>
                            <td class="text-center font-size-sm">{{ $Average->id }}</td>
                            <td>@if(isset($Average->promedio_ml)) {{$Average->RealPromedioML}} ({{$Average->qty_ml}}) @else Status Error @endif</td>
                            <td>@if(isset($Average->promedio_motor)) {{$Average->RealPromedioMotor}} @else Batch Error @endif</td>
                            <td>@if(isset($Average->modelo)) {{$Average->modelo}} @else Batch Error @endif</td>
                            <td>@if(isset($Average->year)) {{$Average->year}} @else Batch Error @endif</td>
                            <td>
                                @if(isset($Average->version_id)) 
                                @if(isset($Average->version->name))
                                {{$Average->version->name}}
                                @else
                                Version Error 
                                @endif
                                @else 
                                Version Error 
                                @endif
                            </td>
                            <td>

                                <!--  <a href="/dash/averages/{{$Average->id}}/edit"  class="btn btn-sm btn-outline-success">Ver</a> -->                            
                                <form action="/dash/averages/{{$Average->id}}" method="POST" style="    display: inline-block;">
                                    @method('DELETE')
                                    {{ csrf_field() }}
                                    <a  href="" class="delete_button btn btn-sm btn-outline-danger">Eliminar</a>
                                </form>
                            </td>
                        </tr> 
                        @empty
                        <tr>
                            <td class="text-center font-size-sm">#</td>
                            <td class="font-w600 font-size-sm">
                                <a >No hay promedios registradas</a>
                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                        </tr>
                        @endforelse 
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Dynamic Table Full -->
    </div>
    <!-- END Page Content --> 
@endsection
@section('js_after')
<script src="/js/custom-mia-dataTable.js"></script>  
<script type="text/javascript">
    $(document).ready(function () {
        'use strict';  
        var oTables = smartInitDateTable(); 
    });
</script> 
@endsection