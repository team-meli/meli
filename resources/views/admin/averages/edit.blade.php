@extends('layouts.backend')

@section('content')
    <!-- Hero -->
<div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">Publicacion</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">
                            <a href="/dash/" class="link-fx btn btn-sm btn-outline-primary">Todas las publicaciones</a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx btn btn-sm btn-primary" href="/dash/publications/create">Realizar nueva busqueda</a>
                        </li>
                    </ol>
                </nav>
            </div>
       </div>
    </div>
    <!-- END Hero -->

 <!-- Page Content -->
<div class="content">


    <!-- Alternative Style -->
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">Registro de Publicacion</h3>
        </div>
        <div class="block-content block-content-full">
            @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            @endif



            <div class="row">
                <div class="col-lg-4">
                    <p class="font-size-sm text-muted">
                        Completa los datos de la Scooter
                    </p>
                </div>

                <div class="col-lg-8 col-xl-5">
                    <div class="form-group">
                        <label for="name">title</label>
                        <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="title" placeholder="title" value="{{ old('title', $Publication->title) }}">
                    </div>

                    <div class="form-group">
                        <label for="name">idML</label>
                        <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="idML" placeholder="idML" value="{{ old('idML', $Publication->idML) }}">
                    </div>
                    <div class="form-group">
                        <label for="name">price</label>
                        <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="price" placeholder="price" value="{{ old('price', $Publication->price) }}">
                    </div>
                    <div class="form-group">
                        <label for="name">listing_type_id</label>
                        <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="listing_type_id" placeholder="listing_type_id" value="{{ old('listing_type_id', $Publication->listing_type_id) }}">
                    </div>
                    <div class="form-group">
                        <label for="name">stop_time</label>
                        <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="stop_time" placeholder="COD" value="{{ old('stop_time', $Publication->stop_time) }}">
                    </div>
                    <div class="form-group">
                        <label for="name">condition</label>
                        <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="cod" placeholder="condition" value="{{ old('condition', $Publication->condition) }}">
                    </div>
                    <div class="form-group">
                        <label for="name">permalink</label>
                        <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="permalink" placeholder="permalink" value="{{ old('permalink', $Publication->permalink) }}">
                    </div>
                    <div class="form-group">
                        <label for="name">thumbnail</label>
                        <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="thumbnail" placeholder="thumbnail" value="{{ old('thumbnail', $Publication->thumbnail) }}">
                    </div>
                    <div class="form-group">
                        <label for="name">state_name</label>
                        <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="state_name" placeholder="state_name" value="{{ old('state_name', $Publication->state_name) }}">
                    </div>
                    <div class="form-group">
                        <label for="name">city_name</label>
                        <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="city_name" placeholder="city_name" value="{{ old('city_name', $Publication->city_name) }}">
                    </div>
                    <div class="form-group">
                        <label for="name">original_price</label>
                        <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="original_price" placeholder="original_price" value="{{ old('original_price', $Publication->original_price) }}">
                    </div>
                    <div class="form-group">
                        <label for="name">available_quantity</label>
                        <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="available_quantity" placeholder="available_quantity" value="{{ old('available_quantity', $Publication->available_quantity) }}">
                    </div>
                    <div class="form-group">
                        <label for="name">sold_quantity</label>
                        <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="sold_quantity" placeholder="sold_quantity" value="{{ old('sold_quantity', $Publication->sold_quantity) }}">
                    </div>
                    <div class="form-group">
                        <label for="name">buying_mode</label>
                        <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="buying_mode" placeholder="buying_mode" value="{{ old('buying_mode', $Publication->buying_mode) }}">
                    </div>
                    <div class="form-group">
                        <label for="name">ITEM_CONDITION</label>
                        <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="ITEM_CONDITION" placeholder="ITEM_CONDITION" value="{{ old('ITEM_CONDITION', $Publication->ITEM_CONDITION) }}">
                    </div>
                    <div class="form-group">
                        <label for="name">BRAND</label>
                        <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="BRAND" placeholder="BRAND" value="{{ old('BRAND', $Publication->BRAND) }}">
                    </div>
                    <div class="form-group">
                        <label for="name">DOORS</label>
                        <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="DOORS" placeholder="DOORS" value="{{ old('DOORS', $Publication->DOORS) }}">
                    </div>
                    <div class="form-group">
                        <label for="name">FUEL_TYPE</label>
                        <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="FUEL_TYPE" placeholder="FUEL_TYPE" value="{{ old('FUEL_TYPE', $Publication->FUEL_TYPE) }}">
                    </div>
                    <div class="form-group">
                        <label for="name">KILOMETERS</label>
                        <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="KILOMETERS" placeholder="KILOMETERS" value="{{ old('KILOMETERS', $Publication->KILOMETERS) }}">
                    </div>
                    <div class="form-group">
                        <label for="name">MODEL</label>
                        <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="MODEL" placeholder="MODEL" value="{{ old('MODEL', $Publication->MODEL) }}">
                    </div>
                    <div class="form-group">
                        <label for="name">TRANSMISSION</label>
                        <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="TRANSMISSION" placeholder="TRANSMISSION" value="{{ old('TRANSMISSION', $Publication->TRANSMISSION) }}">
                    </div>
                    <div class="form-group">
                        <label for="name">TRIM</label>
                        <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="TRIM" placeholder="TRIM" value="{{ old('TRIM', $Publication->TRIM) }}">
                    </div>
                    <div class="form-group">
                        <label for="name">VEHICLE_YEAR</label>
                        <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="VEHICLE_YEAR" placeholder="VEHICLE_YEAR" value="{{ old('VEHICLE_YEAR', $Publication->VEHICLE_YEAR) }}">
                    </div>
                    <div class="form-group">
                        <label for="name">TRACTION_CONTROL</label>
                        <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="TRACTION_CONTROL" placeholder="TRACTION_CONTROL" value="{{ old('TRACTION_CONTROL', $Publication->TRACTION_CONTROL) }}">
                    </div>
                  
                    <button type="success" class="btn btn-primary"><a href="/dash/">Regresar</a></button>
                </div>
            </div>

        </div>
    </div>
    <!-- END Alternative Style -->

</div>
<!-- END Page Content -->


@endsection
