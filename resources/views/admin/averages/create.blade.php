@extends('layouts.backend')

@section('content')
    <!-- Hero -->
<div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">Promedios</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">
                            <a href="/dash/averages/" class="link-fx btn btn-sm btn-outline-primary">Todas los promedios</a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx btn btn-sm btn-primary" href="/dash/averages/create">Registrar un promedio</a>
                        </li>
                    </ol>
                </nav>
            </div>
       </div>
    </div>
    <!-- END Hero -->

 <!-- Page Content -->
<div class="content">


    <!-- Alternative Style -->
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">Crear promedio</h3>
        </div>
        <div class="block-content block-content-full">
            @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            @endif



            <form action="/dash/averages" method="POST" validate=true>
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-lg-4">
                        <p class="font-size-sm text-muted">
                            Completa los datos para crear promedio
                        </p>
                    </div>
                    <div class="col-lg-8 col-xl-5">
                        <div class="form-group">
                            <label for="name">Promedio de Motor</label>
                            <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="promedio_motor" placeholder="Promedio dado por la revista Motor" value="{{ old('promedio_motor') }}">
                        </div>
                        <div class="form-group">
                            <label for="name">Año</label>
                            <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="year" placeholder="Año a calcular promedio" value="{{ old('year') }}">
                        </div>
                        <div class="form-group">
                            <label for="name">Marca</label>
                            <select name="brand" id="brand" class="js-select2 form-control">
                                <option value="0">Selecciona una Marca</option>
                            @foreach($Brands as $Brand)
                                <option value="{{$Brand['value_name']}}">{{$Brand['value_name']}}</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="name">Modelo</label>
                            <select name="model" id="model" class="js-select2 form-control">
                                <option value="0">Selecciona una marca primero</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="name">Versión</label>
                            <select name="version" id="version" class="js-select2 form-control">
                                <option value="0">Selecciona un modelo primero</option>
                            </select>
                        </div>

                     
                        <button type="success" class="btn btn-primary">Crear Promedio</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END Alternative Style -->

</div>
<!-- END Page Content -->


@endsection
@section('js_after')

<script type="text/javascript">

    $(function(){

        $("#brand").change(function(){
            $('#model').html('<option value="0">Cargando</option>');
            var brand = $(this).children("option:selected").val();
            $.ajaxSetup({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
            });
            $.ajax({
                url: "/dash/models/brand/"+brand+"",
                method: 'GET',
                data: {
                    brand: brand,
                },
                success: function(result){
                    console.log(result);
                    $('#model').html('<option value="0">Selecciona un modelo</option>');
                    var models = JSON.parse(result);
                    $.each( models, function( key, value ) {
                        $('#model').append(new Option(value, value));
                    });
                }
            });

        });

        $("#model").change(function(){
            $('#version').html('<option value="0">Cargando</option>');
            var model = $(this).children("option:selected").val();
            $.ajaxSetup({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
            });
            $.ajax({
                url: "/dash/versions/model/"+model+"",
                method: 'GET',
                data: {
                    model: model,
                },
                success: function(result){
                    console.log(result);
                    $('#version').html('<option value="0">Selecciona una versión</option>');
                    var models = JSON.parse(result);
                    $.each( models, function( key, value ) {
                        $('#version').append(new Option(value.name, value.id));
                    });
                }
            });


        });

    });

</script>

@endsection