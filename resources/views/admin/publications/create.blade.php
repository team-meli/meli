@extends('layouts.backend')

@section('content')
    <!-- Hero -->
<div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">Busqueda</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">
                            <a href="/dash/" class="link-fx btn btn-sm btn-outline-primary">Hacer una búsqueda</a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx btn btn-sm btn-primary" href="/dash/publications/create">Crear una Consulta (ML)</a>
                        </li>
                    </ol>
                </nav>
            </div>
       </div>
    </div>
    <!-- END Hero -->

 <!-- Page Content -->
<div class="content">


    <!-- Alternative Style -->
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">Nueva Busqueda</h3>
        </div>
        <div class="block-content block-content-full">
            @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            @endif



            <form action="/dash/publications" method="POST" validate=true>
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-lg-4">
                        <p class="font-size-sm text-muted">
                            Completa los datos de la Busqueda
                        </p>
                    </div>
                    <div class="col-lg-8 col-xl-5">
                        <div class="form-group">
                            <label for="name">Token</label>
                            <input type="text" class="form-control form-control-alt" id="token" name="token" placeholder="Ej: APP_USR-6092-3246532-cb45c82853f6e620bb0deda096b128d3-8035443" value="{{ old('token') }}">
                        </div>
                        <div class="form-group">
                            <label for="name">Cantidad</label>
                            <input type="text" class="form-control form-control-alt" id="cant" name="cant" placeholder="Ej: 50" value="{{ old('cant') }}">
                        </div>
                        <div class="form-group">
                            <label for="name">Offset</label>
                            <input type="text" class="form-control form-control-alt" id="offset" name="offset" placeholder="Ej: 50" value="{{ old('offset') }}">
                        </div>
                        <div class="form-group">
                            <label for="name">Marca</label>
                            <select name="marca" id="marca" class="js-select2 form-control">
                                <option value="0">Selecciona una marca</option>
                                <option value="MCO5782">Audi</option>
                                <option value="MCO5783">BMW</option>
                                <option value="MCO3185">Chevrolet</option>
                                <option value="MCO5779">Citroën</option>
                                <option value="MCO6672">Dodge</option>
                                <option value="MCO3174">Fiat</option>
                                <option value="MCO3180">Ford</option>
                                <option value="MCO5739">Honda</option>
                                <option value="MCO5683">Hyundai</option>
                                <option value="MCO6600">Jeep</option>
                                <option value="MCO7079">Kia</option>
                                <option value="MCO8125">Land Rover</option>
                                <option value="MCO10357">Lexus</option>
                                <option value="MCO5681">Mazda</option>
                                <option value="MCO6038">Mercedes Benz</option>
                                <option value="MCO8480">Mini</option>
                                <option value="MCO5743">Mitsubishi</option>
                                <option value="MCO6173">Nissan</option>
                                <option value="MCO5748">Peugeot</option>
                                <option value="MCO181120">RAM</option>
                                <option value="MCO3205">Renault</option>
                                <option value="MCO6041">Rover</option>
                                <option value="MCO6109">Seat</option>
                                <option value="MCO11927">Ssangyong</option>
                                <option value="MCO7078">Subaru</option>
                                <option value="MCO6583">Suzuki</option>
                                <option value="MCO5753">Toyota</option>
                                <option value="MCO3196">Volkswagen</option>
                                <option value="MCO7080">Volvo</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="name">Busqueda recursiva</label><br>
                            <small>Si es recursiva, busca todos los resultados actuales en ML, pero se tarda unos minutos en procesarlos.</small><br>
                            <select name="recursiva" id="recursiva" class="js-select2 form-control">
                                <option value="0">NO</option>
                                <option value="1">SI</option> 
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <label for="name">Todas las Marcas</label><br>
                            <small>Si le das a esta opción, preparate un café, por que tardará varios minutos.</small><br>
                            <select name="allbrands" id="allbrands" class="js-select2 form-control">
                                <option value="0">NO</option>
                                <option value="1">SI</option> 
                            </select>
                        </div>
                        <button type="success" class="btn btn-primary">Realizar busqueda</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END Alternative Style -->

</div>
<!-- END Page Content -->


@endsection
