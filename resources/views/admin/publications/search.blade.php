@extends('layouts.backend')

@section('content')
    <!-- Hero -->
    <!-- END Hero -->

 <!-- Page Content -->
<div class="content">


    <!-- Alternative Style -->
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">Buscar Publicaciones</h3>
        </div>
        <div class="block-content block-content-full">
            @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            @endif



            <form action="/dash/publications/search" method="POST" validate=true>
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-lg-4">
                        <p class="font-size-sm text-muted">
                            Escoge Marca, Modelo y Versión para buscar resultados. 
                        </p>
                    </div>
                    <div class="col-lg-8 col-xl-5">
                        <div class="form-group">
                            <label for="name">Marcas</label>
                            <select name="brand" id="brand" class="js-select2 form-control">
                                <option value="0">Selecciona una marca</option>
                            @foreach($Marcas as $Marca)
                                <option value="{{$Marca['value_name']}}" >{{$Marca['value_name']}}</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="name">Modelo</label>
                            <select name="model" id="model" class="js-select2 form-control">
                                <option value="0">Selecciona una marca primero</option>
                            </select>
                        </div>                     
                        <button type="success" class="btn btn-primary">Buscar Publicaciones</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END Alternative Style -->

</div>
<!-- END Page Content -->


@endsection
@section('js_after')

<script type="text/javascript">

    $(function(){


        $("#brand").change(function(){
            $('#model').html('<option value="0">Cargando</option>');
            var brand = $(this).children("option:selected").val();
            $.ajaxSetup({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
            });
            $.ajax({
                url: "/dash/models/brand/"+brand+"",
                method: 'GET',
                data: {
                    brand: brand,
                },
                success: function(result){
                    console.log(result);
                    $('#model').html('<option value="0">Selecciona un modelo</option>');
                    var models = JSON.parse(result);
                    $.each( models, function( key, value ) {
                        $('#model').append(new Option(value, value));
                    });
                }
            });

        });

    });

</script>

@endsection