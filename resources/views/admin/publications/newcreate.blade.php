@extends('layouts.backend')

@section('content')
    <!-- Hero -->
<div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">Busqueda</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">
                            <a href="/dash/" class="link-fx btn btn-sm btn-outline-primary">Hacer una búsqueda</a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx btn btn-sm btn-primary" href="/dash/publications/create">Crear una Consulta (ML)</a>
                        </li>
                    </ol>
                </nav>
            </div>
       </div>
    </div>
    <!-- END Hero -->

 <!-- Page Content -->
<div class="content">


    <!-- Alternative Style -->
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">Nueva Busqueda</h3>
        </div>
        <div class="block-content block-content-full">
            @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            @endif



            <form action="/dash/publications" method="POST" validate=true>
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-lg-4">
                        <p class="font-size-sm text-muted">
                            Completa los datos de la Busqueda
                        </p>
                    </div>
                    <div class="col-lg-8 col-xl-5">
<!--                         <div class="form-group">
                            <label for="name">Cantidad</label>
                            <input type="text" class="form-control form-control-alt" id="cant" name="cant" placeholder="Ej: 50" value="{{ old('cant') }}">
                        </div>
                        <div class="form-group">
                            <label for="name">Offset</label>
                            <input type="text" class="form-control form-control-alt" id="offset" name="offset" placeholder="Ej: 50" value="{{ old('offset') }}">
                        </div> -->
                        <div class="form-group">
                            <label for="name">Marca</label>
                            <select name="brand" id="brand" class="js-select2 form-control">
                                <option value="0">Selecciona una marca</option>
                                <option value="MCO5782">Audi</option>
                                <option value="MCO5783">BMW</option>
                                <option value="MCO3185">Chevrolet</option>
                                <option value="MCO5779">Citroën</option>
                                <option value="MCO6672">Dodge</option>
                                <option value="MCO3174">Fiat</option>
                                <option value="MCO3180">Ford</option>
                                <option value="MCO5739">Honda</option>
                                <option value="MCO5683">Hyundai</option>
                                <option value="MCO6600">Jeep</option>
                                <option value="MCO7079">Kia</option>
                                <option value="MCO8125">Land Rover</option>
                                <option value="MCO10357">Lexus</option>
                                <option value="MCO5681">Mazda</option>
                                <option value="MCO6038">Mercedes Benz</option>
                                <option value="MCO8480">Mini</option>
                                <option value="MCO5743">Mitsubishi</option>
                                <option value="MCO6173">Nissan</option>
                                <option value="MCO5748">Peugeot</option>
                                <option value="MCO181120">RAM</option>
                                <option value="MCO3205">Renault</option>
                                <option value="MCO6041">Rover</option>
                                <option value="MCO6109">Seat</option>
                                <option value="MCO11927">Ssangyong</option>
                                <option value="MCO7078">Subaru</option>
                                <option value="MCO6583">Suzuki</option>
                                <option value="MCO5753">Toyota</option>
                                <option value="MCO3196">Volkswagen</option>
                                <option value="MCO7080">Volvo</option>
                            </select>
                        </div>
<!--                         <div class="form-group">
                            <label for="name">Busqueda recursiva</label><br>
                            <small>Si es recursiva, busca todos los resultados actuales en ML, pero se tarda unos minutos en procesarlos.</small><br>
                            <select name="recursiva" id="recursiva" class="js-select2 form-control">
                                <option value="0">NO</option>
                                <option value="1">SI</option> 
                            </select>
                        </div> -->
                        
                        <div class="form-group">
                            <label for="name">Todas las Marcas</label><br>
                            <small>Si le das a esta opción, preparate un café, por que tardará varios minutos.</small><br>
                            <select name="allbrands" id="allbrands" class="js-select2 form-control">
                                <option value="0">NO</option>
                                <option value="1">SI</option> 
                            </select>
                        </div>
                        <a id="searchpublications" onclick="javascript:clickandsearch();" class="btn btn-success" style="color: white;">Realizar busqueda</a>

                        <a id="stopsearching" onclick="javascript:stopsearching(false);" class="btn btn-danger hide" style="color: white;">Parar busqueda</a>

                        <div id="oldresult">
                            
                        </div>
                        <div id="result" offset="0" lastbrand="">
                            
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END Alternative Style -->

</div>
<!-- END Page Content -->


@endsection

@section('js_after') 
    <script>

var searching = true; 
var thebrands = ['MCO5783','MCO3185','MCO5779','MCO6672','MCO3174','MCO3180','MCO5739','MCO5683','MCO6600','MCO7079','MCO8125','MCO10357','MCO5681','MCO6038','MCO8480','MCO5743','MCO6173','MCO5748','MCO181120','MCO3205','MCO6041','MCO6109','MCO11927','MCO7078','MCO6583','MCO5753','MCO3196','MCO7080'];
var indice = 0;

function stopsearching(finalizada){
    searching = false; 
    if(finalizada){
        $('#result').append('<br> Búsqueda Finalizada');  
    }else{
        $('#result').append('<br> Búsqueda Detenida');
    }
     
     $('#searchpublications').removeClass('disabled');
     $('#searchpublications').html('Realizar búsqueda');
     $('#stopsearching').addClass('hide');
}

function clickandsearch(){
    if(!searching){
        $('#result').append('<br> Retomando búsqueda');
    }
    $('#searchpublications').addClass('disabled');
    $('#searchpublications').html('<i class="fas fa-sync fa-spin"></i>');
    $('#stopsearching').removeClass('hide');
    searching = true;
    searchpublications();
}

function searchpublications(){

    var csrt = $('input[name="_token"]').val(); 
    var allbrands = $('#allbrands').val();

    var searchingbrand = $('#brand').val();
    if(allbrands==1){
        searchingbrand = thebrands[indice];
        console.log(indice);
    }

    var formData = {
        'offset': $('#result').attr('offset'),
        'lastbrand': $('#result').attr('lastbrand'),
        'brand': searchingbrand,
        '_token': csrt
    };

    console.log(formData);
  
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': csrt
        }
    });

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': csrt
        },
        url: "/dash/searchpublications",
        method: 'POST',
        data: formData,
        success: function(result){
            console.log(result);
            var lastbrand = result[0];
            var totalmod = result[1].total;
            // totalmod = 50; // DEBUG
            if(totalmod>1000){
                totalmod = 950;
            }
            var totalbrand = totalmod;//
            var qtyloop = 100;//result[1].limit
            var offsetloop = result[1].offset;
            var offset = parseFloat(offsetloop)+parseFloat(qtyloop);
            if(offset>totalmod){
                offset = totalmod;
            }
            $('#result').attr('offset',offset);
            $('#result').attr('lastbrand',lastbrand);
            $('#result').html('Procesando Marca: '+lastbrand+' <br> Resultados:  '+offset+'/'+totalbrand);

            if(offset<totalbrand){
                setTimeout(function(){ 
                    if(searching){
                        searchpublications();
                    }
                }, 2000);
            }else{
                if(allbrands==1){
                    console.log('SIGUIENTE MARCA');
                    indice++;
                    $('#result').attr('offset',0);
                    $('#oldresult').append('<br>'+$('#result').html());
                    $('#result').html('');

                    setTimeout(function(){ 
                        if(searching && indice<28){
                            searchpublications();
                        }else{
                            stopsearching(true);
                        }
                    }, 2000);
                }else{
                    stopsearching(true);
                }
            }

        }
    });

}


    </script>
@endsection