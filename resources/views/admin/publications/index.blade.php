@extends('layouts.backend')

@section('content')
    <link rel="stylesheet" type="text/css" href="/css/custom-mia-dataTable.css">
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">
                    @if(isset($Groupname))
                        {{$Groupname}}
                    @else
                        Publicaciones 
                    @endif
                </h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
<!--                     <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">
                            <a href="/dash/search/" class="link-fx btn btn-sm btn-outline-primary">Hacer una búsqueda</a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx btn btn-sm btn-primary" href="/dash/publication/newcreate">Crear una Consulta (ML)</a>
                        </li>
                    </ol> -->
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

     <!-- Page Content -->
    <div class="content" style="    overflow:hidden;">
        <!-- Dynamic Table Full -->
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Publicaciones <small></small></h3>
            </div>
            <div class="block-content block-content-full">
                <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                @if (session('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                @endif
                <!-- Bloque de opciones --> 
                <div class="block-options-dataTable">
                    <div class="tittle-option">Vistas</div>
                    <div class="buttons-option">
                        <button href="#" class="btn btn-sm btn-outline-danger"  
                            custom-filter='[Retraso|moroso]' 
                            custom-filter-column='7' 
                            custom-sort='asc' 
                            custom-sort-column='6'>
                            Cartera Vencida
                        </button>                  
                    </div>
                </div> 

                <table class="table table-bordered table-striped table-vcenter init-dataTable">
                    <thead>
                        <tr>
                            <th filter-type ='range_number_slider'> <span class="title-head">Barometro</span> </th>
                            <th filter-type ='multi_select'> <span class="title-head">Marca</span> </th>
                            <th filter-type ='multi_select'> <span class="title-head">Modelo</span> </th>
                            <th filter-type ='text'> <span class="title-head">Versión</span> </th>
                            <th filter-type ='multi_select'> <span class="title-head">Año</span> </th>
                            <th filter-type ='range_number_slider'> <span class="title-head">TOTAL ML</span> </th>
                            <th filter-type ='range_number_slider'> <span class="title-head">ML KM</span> </th>
                            <th filter-type ='range_number'> <span class="title-head">%KM</span> </th>
                            <th filter-type ='range_number'> <span class="title-head">Rango KM</span> </th>
                            <th filter-type ='multi_select'> <span class="title-head">Dpto.</span> </th>
                            <th filter-type ='range_number_slider'> <span class="title-head">Precio</span> </th>
                            <th> <span class="title-head">Promedio de la version/año</span> </th>
                            <th> <span class="title-head">Promedio del rango KM</span> </th>
                            <th filter-type ='range_number_slider'> <span class="title-head">MOTOR</span> </th>
                            <th> <span class="title-head">Promedio Motor</span> </th>
                            <th> <span class="title-head">Kilometraje Esperado</span> </th>
                            
                            <th >Acciones</th>
                        </tr>
                    </thead> 
                    <tbody>
                        @forelse($Publications as $Publication)
                            <tr>
                                <td class="font-size-sm">
                                    <br>
                                    <span>@if(isset($Publication->barometro)) {{$Publication->barometro}} @else ml_average_tooltip Error @endif</span>
                                </td>
                                <td class="font-w600 font-size-sm">
                                    <span style="font-size: 11px">@if(isset($Publication->BRAND)) {{$Publication->BRAND }} @else Brand Error @endif</span>
                                </td>
                                <td class="font-w600 font-size-sm">
                                    <span style="font-size: 11px">@if(isset($Publication->MODEL)) {{$Publication->MODEL }} @else Model Error @endif</span>
                                </td>
                                <td class="font-size-sm">
                                    <span style="font-size: 10px">@if(isset($Publication->RealVersion)) @if(isset($Publication->JustTrim)) {{$Publication->JustTrim }} @else -- @endif  @else   Version Error   @endif</span>
                                </td>
                                <td class="font-size-sm">
                                    <span>@if(isset($Publication->VEHICLE_YEAR)) {{$Publication->VEHICLE_YEAR }}  @else  Year Error   @endif</span>
                                </td> 
                                <td class="font-size-sm">
                                    <span>
                                    @if(isset($Publication->histories->last()->ml_average))
                                        @if($Publication->histories->last()->ml_average!="N/A")
                                        {{$Publication->histories->last()->ml_average }}% 
                                        @else
                                        0%
                                        @endif
                                    @else  
                                        ml_average Error 
                                    @endif
                                    </span>
                                </td>
                                <td class="font-size-sm"> 
                                    <span>
                                    @if(isset($Publication->histories->last()->ml_average_km))
                                        @if($Publication->histories->last()->ml_average_km!="N/A")
                                        {{$Publication->histories->last()->ml_average_km }}% 
                                        @else
                                        0%
                                        @endif
                                    @else  
                                        ml_average_km Error 
                                    @endif
                                    </span>
                                </td>
                                
                                <td class="font-size-sm">
                                    <span>
                                    @if(isset($Publication->histories->last()->average_expected_km))
                                        @if($Publication->histories->last()->average_expected_km!="N/A")
                                        {{$Publication->histories->last()->average_expected_km }}% 
                                        @else
                                        0%
                                        @endif
                                    @else  
                                        average_expected_km Error 
                                    @endif
                                    </span>
                                </td>
                                <td class="font-size-sm">
                                    <span>@if(isset($Publication->RealRange)) {{$Publication->RealRange}} @else RealRange Error @endif</span>
                                </td>
                                <td class="font-size-sm">
                                    <span style="font-size: 10px">@if(isset($Publication->state_name)) {{$Publication->state_name}} @else state_name Error @endif</span>
                                </td>
                                <td class="font-size-sm">
                                    <span style="font-size: 10px">@if(isset($Publication->price)) {{$Publication->Realprice}} @else Price Error @endif</span>
                                </td>
                                <td class="font-size-sm">
                                    <br>
                                    <span>@if(isset($Publication->histories->last()->ml_average_tooltip)) {!!$Publication->histories->last()->ml_average_tooltip!!} @else ml_average_tooltip Error @endif</span>
                                </td>
                                <td class="font-size-sm">
                                    <br>
                                    <span>@if(isset($Publication->histories->last()->ml_average_km_tooltip)) {!!$Publication->histories->last()->ml_average_km_tooltip!!} @else ml_average_tooltip Error @endif</span>
                                </td>
                                <td class="font-size-sm">
                                    <span>@if(isset($Publication->histories->last()->motor_average)) {{$Publication->histories->last()->motor_average }}% @else  motor_average Error  @endif</span>
                                </td>
                                <td class="font-size-sm">
                                    <br>
                                    <span>@if(isset($Publication->histories->last()->motor_average_tooltip)) {!!$Publication->histories->last()->motor_average_tooltip!!} @else ml_average_tooltip Error @endif</span>
                                </td>
                                <td class="font-size-sm">
                                    <br>
                                    <span>@if(isset($Publication->histories->last()->average_expected_km_tooltip)) {!!$Publication->histories->last()->average_expected_km_tooltip!!} @else ml_average_tooltip Error @endif</span>
                                </td>

                                <td style="">
                                    <br>
                                    @if(isset($Publication->permalink)) 
                                    <br>
                                        <a   style="width: 200px" class="btn btn-success" href="{{$Publication->permalink}}" target="_blank">Mercado Libre</a> 
                                    @else @endif
                                    <br>
                                    <br>
                                     <!-- <a href="/dash/publications/{{$Publication->id}}/edit"  style="width: 200px"   class="btn btn-warning">VER</a> -->
                                    <a href="/dash/publication/{{$Publication->id}}/deleting" style="width: 200px" class=" btn btn-danger">Eliminar</a> 
                                    <br><br>
                                    <a href="/dash/publication/{{$Publication->id}}/sold" style="width: 200px" class=" btn btn-warning">Vendido</a> 

                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td class="text-center font-size-sm">#</td>
                                <td class="font-w600 font-size-sm"><a >No hay adopciones registradas</a></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        @endforelse
                    </tbody>
                </table> 
            </div>
        </div>
        <!-- END Dynamic Table Full -->
    </div>
    <!-- END Page Content --> 
@endsection 
@section('js_tabla')
    <script src="/js/custom-mia-dataTable.js"></script>  
    <script type="text/javascript">
        $(document).ready(function () {
            'use strict';   
            var oTables = smartInitDateTable(); 
        });
    </script> 
@endsection