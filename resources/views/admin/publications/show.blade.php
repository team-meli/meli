@extends('layouts.backend')

@section('content')
<link rel="stylesheet" type="text/css" href="/css/custom-mia-dataTable.css">
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">Publicacion</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">
                            <a href="/dash/" class="link-fx btn btn-sm btn-outline-primary">Hacer una búsqueda</a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx btn btn-sm btn-primary" href="/dash/publications/create">Crear una Consulta (ML)</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content"> 
        <!-- Alternative Style -->
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Registro de Publicacion</h3>
            </div>
            <div class="block-content block-content-full">
                @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
                @endif
                <div class="row">
                    <div class="col-lg-4">
                        <p class="font-size-sm text-muted">
                            Completa los datos de la Scooter
                        </p>
                    </div>
                    <div class="col-lg-8 col-xl-5">
                        <div class="form-group">
                            <label for="name">Titulo</label>
                            <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="title" placeholder="title" value="{{ old('title', $Publication->title) }}">
                        </div>

                        <div class="form-group">
                            <label for="name">Id Mercado Libre</label>
                            <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="idML" placeholder="idML" value="{{ old('idML', $Publication->idML) }}">
                        </div>
                        <div class="form-group">
                            <label for="name">Precio</label>
                            <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="price" placeholder="price" value="{{ old('price', $Publication->price) }}">
                        </div>
                        <div class="form-group">
                            <label for="name">Tipo de listado</label>
                            <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="listing_type_id" placeholder="listing_type_id" value="{{ old('listing_type_id', $Publication->listing_type_id) }}">
                        </div>
                        <div class="form-group">
                            <label for="name">Fecha de cierre</label>
                            <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="stop_time" placeholder="COD" value="{{ old('stop_time', $Publication->stop_time) }}">
                        </div>
                        <div class="form-group">
                            <label for="name">Condición </label>
                            <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="cod" placeholder="condition" value="{{ old('condition', $Publication->condition) }}">
                        </div>
                        <div class="form-group">
                            <label for="name">Link</label>
                            <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="permalink" placeholder="permalink" value="{{ old('permalink', $Publication->permalink) }}">
                        </div>
                        <div class="form-group">
                            <label for="name">Imagen</label>
                            <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="thumbnail" placeholder="thumbnail" value="{{ old('thumbnail', $Publication->thumbnail) }}">
                        </div>
                        <div class="form-group">
                            <label for="name">Estado</label>
                            <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="state_name" placeholder="state_name" value="{{ old('state_name', $Publication->state_name) }}">
                        </div>
                        <div class="form-group">
                            <label for="name">Ciudad</label>
                            <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="city_name" placeholder="city_name" value="{{ old('city_name', $Publication->city_name) }}">
                        </div>
                        <div class="form-group">
                            <label for="name">Precio Original</label>
                            <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="original_price" placeholder="original_price" value="{{ old('original_price', $Publication->original_price) }}">
                        </div>
                        <div class="form-group">
                            <label for="name">Cantidad disponible</label>
                            <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="available_quantity" placeholder="available_quantity" value="{{ old('available_quantity', $Publication->available_quantity) }}">
                        </div>
                        <div class="form-group">
                            <label for="name">Cantidad vendida</label>
                            <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="sold_quantity" placeholder="sold_quantity" value="{{ old('sold_quantity', $Publication->sold_quantity) }}">
                        </div>
                        <div class="form-group">
                            <label for="name">Tipo de venta</label>
                            <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="buying_mode" placeholder="buying_mode" value="{{ old('buying_mode', $Publication->buying_mode) }}">
                        </div>


                        @forelse($Publication->attributes as $attribute)
                        <div class="form-group">
                            <label for="name">{{$attribute->name}}</label>
                            <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="BRAND" placeholder="BRAND" value="{{ old('BRAND', $attribute->pivot->value_name) }}">
                            @if($attribute->value_id=='KILOMETERS')
                            <small>KM Esperados: {{$Publication->ExpectedKM}} ({{$Publication->AverageExpectedKM}})</small>
                            @endif
                        </div>
                        @empty

                        @endforelse

                        <button type="success" class="btn btn-primary"><a href="/dash/publications" style="color: white;">Regresar</a></button>
                    </div>
                </div> 
            </div>
        </div>
        <!-- END Alternative Style -->

        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Historico de Promedios <small></small></h3>
            </div>
            <div class="block-content block-content-full"> 
                @if (session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
                @endif
                <!-- Bloque de opciones --> 
                <div class="block-options-dataTable">
                    <div class="tittle-option">Vistas</div>
                    <div class="buttons-option">
                        <button href="#" class="btn btn-sm btn-outline-danger"  
                            custom-filter='[Retraso|moroso]' 
                            custom-filter-column='7' 
                            custom-sort='asc' 
                            custom-sort-column='6'>
                            Cartera Vencida
                        </button>                  
                    </div>
                </div> 

                <table class="table table-bordered table-striped table-vcenter init-dataTable">
                    <thead>
                        <tr> 
                            <th filter-type='text'>
                                <a class="js-tooltip-enabled" data-toggle="tooltip" data-placement="top" title="" data-original-title="Promedio de vehiculos del mismo cluster en Mercado Libre">
                                    <span class="title-head">TOTAL ML</span>
                                </a> 
                            </th>
                            <th filter-type='text'>
                                <a class="js-tooltip-enabled" data-toggle="tooltip" data-placement="top" title="" data-original-title="Comparativo sobre Kilometraje">
                                    <span class="title-head">ML KM</span>
                                </a> 
                            </th>
                            <th filter-type='text'>
                                <a class="js-tooltip-enabled" data-toggle="tooltip" data-placement="top" title="" data-original-title="Comparativo vs Revista Motor">
                                    <span class="title-head">MOTOR</span> 
                                </a> 
                            </th>
                            <th filter-type='text'>
                                <a class="js-tooltip-enabled" data-toggle="tooltip" data-placement="top" title="" data-original-title="Porcentaje de KM con respecto al esperado del año">
                                    <span class="title-head">%KM</span> 
                                </a> 
                            </th>
                            <th filter-type='text'>
                                <span class="title-head">Precio</span> 
                            </th>
                            <th><span class="title-head">Fecha</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($Publication->histories as $History)
                        <tr>
                            <td>
                                <a class="js-tooltip-enabled" data-toggle="tooltip" data-placement="top" title="" data-original-title="@if(isset($History->MlAverageTooltip)) {{$History->MlAverageTooltip}} @else @endif">
                                    {{$History->MlAverage }}%
                                </a>
                            </td>
                            <td>
                                <a class="js-tooltip-enabled" data-toggle="tooltip" data-placement="top" title=""  data-html="true" data-original-title="@if(isset($History->MlAverageKMTooltip)) {{$History->MlAverageKMTooltip}} @else @endif"> 
                                    {{$History->MlAverageKM }}%
                                </a>
                            </td>
                            <td>

                                <a class="js-tooltip-enabled" data-toggle="tooltip" data-placement="top" title="" data-original-title="@if(isset($History->MotorAverageTooltip)) {{$History->MotorAverageTooltip}} @else @endif">
                                    {{$History->MotorAverage }}%
                                </a>
                            </td>
                            <td>
                                @if(isset($History->AverageExpectedKM))
                                {{$History->AverageExpectedKM }}%
                                @else 
                                AverageExpectedKM Error
                                @endif
                            </td>

                            <td>@if(isset($History->price)) {{$History->RealPrice}} @else Price Error @endif</td>
                            <td>
                                {{$History->created_at}}
                            </td>


                        </tr>

                        @empty
                        <tr>
                            <td class="text-center font-size-sm">#</td>
                            <td class="font-w600 font-size-sm">
                                <a >No hay publicaciones registradas</a>
                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>           
            </div>
        </div> 
    </div>  
@endsection
@section('js_after')
    <script src="/js/custom-mia-dataTable.js"></script>  
    <script type="text/javascript">
        $(document).ready(function () {
            'use strict'; 
            //INICIAR TABLA 
            var oTables = smartInitDateTable(); 
        });
    </script> 
@endsection