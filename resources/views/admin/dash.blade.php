@extends('layouts.backend')

@section('content')
    <link rel="stylesheet" type="text/css" href="/css/custom-mia-dataTable.css">
    <!-- Hero -->
<div class="bg-image overflow-hidden" style="background-image: url('/media/photos/cobra.jpg');">
    <div class="bg-primary-dark-op">
        <div class="content content-narrow content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center mt-5 mb-2 text-center text-sm-left">
                <div class="flex-sm-fill">
                    <h1 class="font-w600 text-white mb-0 js-appear-enabled animated fadeIn" data-toggle="appear">Dashboard</h1>
                    <h2 class="h4 font-w400 text-white-75 mb-0 js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="250">Bienvenido, {{Auth::user()->name}}</h2>
                </div>
                <div class="flex-sm-00-auto mt-3 mt-sm-0 ml-sm-3">
                    <span class="d-inline-block js-appear-enabled animated fadeIn" data-toggle="appear" data-timeout="350">
<!--                         <a class="btn btn-primary px-4 py-2 js-click-ripple-enabled" data-toggle="click-ripple" href="javascript:void(0)" style="overflow: hidden; position: relative; z-index: 1;">
                            <i class="fa fa-plus mr-1"></i> New Project
                        </a> -->
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
    <!-- END Hero -->

<div class="content content-narrow">
    

<div class="row">
        <div class="col-6 col-md-3 col-lg-6 col-xl-3">
            <a class="block block-rounded block-link-pop border-left border-primary border-4x" href="/dash/publications">
                <div class="block-content block-content-full">
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Descubre</div>
                    <div class="font-size-h2 font-w400 text-dark">Ver Ofertas</div>
                </div>
            </a>
        </div>
        <div class="col-6 col-md-3 col-lg-6 col-xl-3">
            <a class="block block-rounded block-link-pop border-left border-primary border-4x" href="/dash/brands">
                <div class="block-content block-content-full">
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Explora</div>
                    <div class="font-size-h2 font-w400 text-dark">Ver Marcas</div>
                </div>
            </a>
        </div>
        <div class="col-6 col-md-3 col-lg-6 col-xl-3">
            <a class="block block-rounded block-link-pop border-left border-primary border-4x" href="/dash/brands">
                <div class="block-content block-content-full">
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Organiza</div>
                    <div class="font-size-h2 font-w400 text-dark">Taggear</div>
                </div>
            </a>
        </div>
        <div class="col-6 col-md-3 col-lg-6 col-xl-3">
            <a class="block block-rounded block-link-pop border-left border-primary border-4x" href="/dash/users">
                <div class="block-content block-content-full">
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Controla</div>
                    <div class="font-size-h2 font-w400 text-dark">Usuarios</div>
                </div>
            </a>
        </div>
    </div>

</div>
@endsection 