@extends('layouts.backend')

@section('content')
    <link rel="stylesheet" type="text/css" href="/css/custom-mia-dataTable.css">
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">Usuarios</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item"><a href="/dash/users"  class="link-fx btn btn-sm btn-outline-primary">Todos los Usuarios</a></li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx btn btn-sm btn-primary" href="/dash/users/create">Crear un Usuario</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <!-- Dynamic Table Full -->
        <div class="block">
            <div class="block-header">
                <h3 class="block-title">Usuarios <small>Registrados</small></h3>
            </div>
            <div class="block-content block-content-full"> 
                @if (session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
                @endif
                <!-- Bloque de opciones --> 
                <div class="block-options-dataTable">
                    <div class="tittle-option">Vistas</div>
                    <div class="buttons-option">
                        <button href="#" class="btn btn-sm btn-outline-danger"  
                            custom-filter='[Retraso|moroso]' 
                            custom-filter-column='7' 
                            custom-sort='asc' 
                            custom-sort-column='6'>
                            Cartera Vencida
                        </button>                  
                    </div>
                </div> 

                <table class="table table-bordered table-striped table-vcenter init-dataTable">
                    <thead>
                        <tr>
                            <th filter-type='text'><span class="title-head">ID</span></th>
                            <th filter-type='text'><span class="title-head">Nombre</span></th>
                            <th filter-type='text'><span class="title-head">Email</span></th>
                            <th filter-type='text'><span class="title-head">Registro</span></th>
                            <th filter-type='text'><span class="title-head">Scooters</span></th>
                            <th><span class="title-head">Acciones</span></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($users as $user)
                        <tr>
                            <td class="text-center font-size-sm">#{{ $user->id }}</td>
                            <td class="font-w600 font-size-sm">
                                <a>{{ $user->name }}</a>
                            </td>
                            <td class="d-none d-sm-table-cell font-size-sm">
                                {{ $user->email }}
                            </td>
                            <td>
                                <em class="text-muted font-size-sm"><?php echo rand(2, 10); ?> days ago</em>
                            </td>
                            <td>
                                <?php echo rand(2, 10); ?>
                            </td>
                            <td class="d-none d-sm-table-cell">
                                <a href="/dash/users/{{$user->id}}/edit"  class="btn btn-sm btn-outline-success">Editar</a>                            
                                <form action="/dash/users/{{$user->id}}" method="POST" style="    display: inline-block;">
                                    @method('DELETE')
                                    {{ csrf_field() }}
                                    <a  href="" class="delete_button btn btn-sm btn-outline-danger">Eliminar</a>
                                </form>
                            </td>
                        </tr> 
                        @empty
                        <tr>
                            <td class="text-center font-size-sm">#</td>
                            <td class="font-w600 font-size-sm">
                                <a>No hay usuarios registrados</a>
                            </td>
                            <td class="d-none d-sm-table-cell font-size-sm">
                                email@miascooters.com
                            </td>
                            <td class="d-none d-sm-table-cell">
                                typeuser
                            </td>
                            <td>
                                <em class="text-muted font-size-sm"><?php echo rand(2, 10); ?> days ago</em>
                            </td>
                            <td>
                                <?php echo rand(2, 10); ?>
                            </td>
                        </tr>
                        @endforelse 
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Dynamic Table Full -->
    </div>
    <!-- END Page Content --> 
@endsection
@section('js_after')
    <script src="/js/custom-mia-dataTable.js"></script>  
    <script type="text/javascript">
        $(document).ready(function () {
            'use strict';  
            var oTables = smartInitDateTable(); 
        });
    </script> 
@endsection