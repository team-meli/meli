@extends('layouts.backend')

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">Usuarios</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item"><a href="/dash/users"  class="link-fx btn btn-sm btn-outline-primary">Todos los Usuarios</a></li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx btn btn-sm btn-primary" href="/dash/users/create">Crear un Usuario</a>
                        </li>
                    </ol>
                </nav>
            </div>
       </div>
    </div>
    <!-- END Hero -->

 <!-- Page Content -->
<div class="content">


    <!-- Alternative Style -->
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">Nuevo Usuario</h3>
        </div>
        <div class="block-content block-content-full">
            @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            @endif



            <form action="/dash/users" method="POST" validate=true>
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-lg-4">
                        <p class="font-size-sm text-muted">
                            Completa los datos del usuario
                        </p>
                    </div>
                    <div class="col-lg-8 col-xl-5">
                        <div class="form-group">
                            <label for="name">Nombre</label>
                            <input type="text" class="form-control form-control-alt" id="example-text-input-alt" name="name" placeholder="Nombre Apellido" value="{{ old('name') }}">
                        </div>
                        <div class="form-group">
                            <label for="password">Clave</label>
                            <input type="password" class="form-control form-control-alt" id="example-password-input-alt" name="password" placeholder="Password" required>
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                           <input type="email" class="form-control form-control-alt" id="example-text-input-alt" name="email" placeholder="Email"  value="{{ old('email') }}">
                        </div>
                        <button type="success" class="btn btn-primary">Crear Usuario</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END Alternative Style -->

</div>
<!-- END Page Content -->


@endsection
