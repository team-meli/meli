@extends('layouts.backend')

@section('content')
    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill h3 my-2">Dashboard</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb breadcrumb-alt">
                        <li class="breadcrumb-item">App</li>
                        <li class="breadcrumb-item" aria-current="page">
                            <a class="link-fx" href="">Dashboard</a>
                        </li>
                    </ol>
                </nav>
            </div>
       </div>
    </div>
    <!-- END Hero -->

 <!-- Page Content -->
<div class="content">
    <!-- Dynamic Table Full -->
    <div class="block">
        <div class="block-header">
            <h3 class="block-title">Consultas</h3>
        </div>
        <div class="block-content block-content-full">
            <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
            @if (session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif
            <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 80px;">ID</th>
                        <th>Nombre</th>
                        <th>Duración</th>
                        <th>Precio</th>
                        <th style="width:180px;">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                @forelse($Plans as $Plan)
                    <tr>
                        <td class="text-center font-size-sm">{{ $Plan->id }}</td>
                        <td class="font-w600 font-size-sm">
                            <a>{{ $Plan->name }}</a>
                        </td>
                        <td>{{ $Plan->duration }}
                            @if($Plan->range == 1) Días @endif
                            @if($Plan->range == 2) Semanas @endif
                            @if($Plan->range == 3) Meses @endif
                        </td>
                        
                        <td>{{ $Plan->price }}</td>
                        <td>
                            <a href="/dash/plans/{{$Plan->id}}/edit"  class="btn btn-sm btn-outline-success">Editar</a>                            
                            <form action="/dash/plans/{{$Plan->id}}" method="POST" style="    display: inline-block;">
                            @method('DELETE')
                            {{ csrf_field() }}
                                <a  href="" class="delete_button btn btn-sm btn-outline-danger">Eliminar</a>
                            </form>
                        </td>
                    </tr>

                @empty
                    <tr>
                        <td class="text-center font-size-sm">#</td>
                        <td class="font-w600 font-size-sm">
                            <a>No hay planes registrados</a>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                @endforelse

                   
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Dynamic Table Full -->
</div>
<!-- END Page Content -->
@endsection
