<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAveragesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('averages');
        Schema::create('averages', function (Blueprint $table) {
            $table->increments('id'); 
            $table->float('promedio_ml', 20, 2);
            $table->float('promedio_motor', 20, 2);
            $table->string('model_id');
            $table->integer('year');
            $table->string('trim');
            $table->integer('qty_ml');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('averages');
    }
}
