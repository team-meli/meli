<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('publications');
        Schema::create('publications', function (Blueprint $table) { 
            $table->increments('id');
            $table->string('id_ML');
            $table->string('title');
            $table->float('price', 20, 2);
            $table->string('listing_type_id');
            $table->date('stop_time')->nullable(); 
            $table->string('permalink');
            $table->string('thumbnail');
            $table->string('state_name');
            $table->string('city_name');
            $table->string('realtrim')->nullable();
            $table->float('original_price', 20, 2)->nullable();
            $table->integer('available_quantity')->nullable();
            $table->integer('sold_quantity')->nullable();
            $table->string('buying_mode')->nullable();
            $table->string('model_id')->nullable();
            $table->string('brand_id')->nullable();
            $table->string('ITEM_CONDITION')->nullable();
            $table->integer('DOORS')->nullable();
            $table->string('FUEL_TYPE')->nullable();
            $table->string('BRAND')->nullable(); 
            $table->string('MODEL')->nullable(); 
            $table->string('KILOMETERS')->nullable();
            $table->string('TRANSMISSION')->nullable();
            $table->string('TRIM')->nullable();
            $table->integer('VEHICLE_YEAR')->nullable();
            $table->string('TRACTION_CONTROL')->nullable();
            $table->string('ENGINE_DISPLACEMENT')->nullable();
            $table->timestamps();
        });
    } 
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('publications');
    }
}
