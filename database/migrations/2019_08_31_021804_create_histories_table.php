<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('histories');
        Schema::create('histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('publication_id');
            $table->float('price', 20, 2); 
            $table->string('ml_average')->nullable();
            $table->string('ml_average_tooltip')->nullable();
            $table->string('ml_average_km')->nullable();
            $table->string('ml_average_km_tooltip')->nullable();
            $table->string('motor_average')->nullable();
            $table->string('motor_average_tooltip')->nullable();
            $table->string('expected_km')->nullable();
            $table->string('average_expected_km')->nullable();
            $table->string('average_expected_km_tooltip')->nullable();
            $table->string('barometro')->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('histories');
    }
}
