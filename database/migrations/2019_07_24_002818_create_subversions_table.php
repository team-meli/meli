<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubversionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('subversions');
        Schema::create('subversions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('version_id'); 
            $table->string('model_id');
            $table->integer('doors');
            $table->string('transmission');   
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subversions');
    }
}
