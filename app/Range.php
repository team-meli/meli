<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Range extends Model
{
    protected $fillable = [
    	'average_id',
    	'min',
    	'max',
    	'average',
    	'qty',
    ];
    public function average()
    {
        return $this->hasOne('App\Average', 'id', 'average_id');
    }
}
