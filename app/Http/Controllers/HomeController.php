<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Version;
use App\Publication;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.dash');
    }


    public function viewAll($brand_id=false)
    {
        if (!$brand_id) {

            $Brands = [];//name,cantPub,cantVersions

            $brands_id = Version::select('brand_id')->distinct()->get();
             
            foreach ($brands_id as $brand_id) {
                $Brand=[];
                $Pubs = Publication::all()->where('brand_id',$brand_id->brand_id);
                $Brand['id']=$brand_id->brand_id;
                $Brand['cantPubs'] = $Pubs->count();
                if($Pubs->count()>0){
                    $Brand['name'] = $Pubs->first()->BRAND;
                }else{
                    $Brand['name'] = $brand_id->brand_id;
                }
                $Brand['cantVersions'] = Version::all()->where('brand_id',$brand_id->brand_id)->count(); 
                array_push($Brands, $Brand);
            }
            return view('admin.brands',compact('Brands'));
        }else{
            $AllModels = Version::select('model_id')->distinct()->where('brand_id',$brand_id)->get();
            $Models=[];
            foreach ($AllModels as $Mod) {
                $Model=[];
                $Model['id']=$Mod->model_id;
                $Pubs = Publication::all()->where('model_id',$Mod->model_id);
                $Model['cantPubs'] = $Pubs->count();
                $ModelVersions = Version::where('model_id',$Mod->model_id)->get();
                $Model['cantVersions'] = Version::all()->where('model_id',$Mod->model_id)->count();
                if($ModelVersions->count()>0){
                    $Model['name'] = $ModelVersions->first()->modelo;
                }else{
                    $Model['name'] = $Mod->model_id;
                }
                array_push($Models, $Model);
            }
            $Pubs = Publication::all()->where('brand_id',$brand_id);
            if($Pubs->count()>0){
                $Brandname = $Pubs->first()->BRAND;
            }else{
                $Brandname = $brand_id->brand_id;
            }

            return view('admin.brands',compact('Models','Brandname'));
        }   
    } 

    public function viewModel($model_id)
    {

            $AllVersions = Version::all()->where('model_id',$model_id);
            $Versions = [];
            $Modelname = $AllVersions->last()->modelo;
            $ModelCant = Publication::where('model_id',$model_id)->count();
            $ModelTag = 0;
            foreach ($AllVersions as $AVersion) {
                $Version = $AVersion;
                $Pubs = Publication::where('realtrim',$Version->RealTrim)->count();
                $Version['taggeados'] = $Pubs;
                $ModelTag += $Pubs;
                array_push($Versions, $Version);
            }
            return view('admin.models',compact('Versions','Modelname','ModelCant','ModelTag'));
          
    }
}
