<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $users = User::all();
        return view('admin.users.index', compact('users') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validator($request,0);
        User::create([
            'name' => request('name'),
            'email' => request()->email,
            'password' => bcrypt(request()->get('password'))
        ]);

        return redirect()->route('users.index')->with('message', 'Usuario creado de forma exitosa');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $User = User::find($id);
        return view('admin.users.edit', compact('User') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validator($request,$id);
        $User = User::find($id);
        $User->name = $request['name'];
        $User->email = $request['email'];

        if($request['password']!=null){
            $User->password = bcrypt($request['password']);
        }

        $User->save();

        return redirect()->route('users.index')->with('message', 'Usuario editado de forma exitosa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $User = User::find($id);
        $User->delete();
        return redirect()->route('users.index')->with('message', 'Usuario eliminado correctamente');
    }

    public function validator($request,$id){
        $passwordvalidate = ($request['password']!=null) ? 'min:8' : '';
        $data = request()->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => $passwordvalidate,
        ], [
            'name.required' => 'El campo nombre es obligatorio',
            'email.required' => 'El campo email es obligatorio',
            'email.email' => 'Por favor ingresa una dirección valida',
            'email.unique' => 'Ya existe un usuario con ese email',
            'password.min' => 'Tu Password es muy corto',
        ]);
    }
}
