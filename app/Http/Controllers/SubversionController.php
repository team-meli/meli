<?php

namespace App\Http\Controllers;

use App\Subversion;
use App\Version;
use Illuminate\Http\Request;

class SubversionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Subversion = Subversion::create([
            'name' => strtoupper(request('name')),
            'version_id' => request('version_id'),

        ]); 
        return redirect('/dash/versions/'.request('version_id').'/edit')->with('message', 'La Subversion fue creada correctamente');
    }


    public function storeAjax(Request $request)
    {
        $doors = $request['doors'];
        $transmission = $request['transmission'];
        $name = $request['name'];
        $version_id = $request['version_id'];
        $Version = Version::find($version_id);

        $Subversion = Subversion::create([
            'name' => $name,
            'version_id' => $version_id,
            'doors' => $doors,
            'transmission' => $transmission,
            'model_id' => $Version->model_id

        ]);
        return $doors.' eeeeeeeadadadad '.$transmission.$version_id.$name;
    }

    public function deleteAjax($version,$name)
    {
        $Subversion = Subversion::all()->where('version_id',$version)->where('name',$name)->last();
        // if ($Subversion) {
            $Subversion->delete();
        // } 
        return 'Eliminada';
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Subversion  $subversion
     * @return \Illuminate\Http\Response
     */
    public function show(Subversion $subversion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subversion  $subversion
     * @return \Illuminate\Http\Response
     */
    public function edit(Subversion $subversion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subversion  $subversion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subversion $subversion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subversion  $subversion
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
        $Subversion = Subversion::find($id);
        $Subversion->delete();
        return redirect()->route('versions.index')->with('message', 'La Subversion fue eliminada correctamente');
    }
}
