<?php

namespace App\Http\Controllers;

use App\Attribute_Publication;
use Illuminate\Http\Request;

class AttributePublicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Attribute_Publication  $attribute_Publication
     * @return \Illuminate\Http\Response
     */
    public function show(Attribute_Publication $attribute_Publication)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Attribute_Publication  $attribute_Publication
     * @return \Illuminate\Http\Response
     */
    public function edit(Attribute_Publication $attribute_Publication)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Attribute_Publication  $attribute_Publication
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Attribute_Publication $attribute_Publication)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Attribute_Publication  $attribute_Publication
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attribute_Publication $attribute_Publication)
    {
        //
    }
}
