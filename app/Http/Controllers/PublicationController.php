<?php

namespace App\Http\Controllers;

use App\Attribute;
use App\Publication;
use App\Version;
use App\Attribute_Publication;
use Illuminate\Http\Request;

class PublicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function index(Request $request)
    {
        set_time_limit(0);
        // $AllPublications = Publication::has('histories')->with('histories')->whereNotIn('sold',[1])->get();
        // $Publications = array();
        // foreach ($AllPublications as $Publication) {
        //     $int = intval($Publication->last_history[0]->ml_average);
        //     if($int<0){
        //         array_push($Publications, $Publication);
        //     }
        //     $Publication->BAROMETER = $Publication->barometro;
        //     $Publication->save();
        // }
        $Publications = Publication::has('histories')->with('histories')->whereNotIn('sold',[1])->take(100)->orderBy('BAROMETER', 'DESC')->get();
        return view('admin.publications.index',compact('Publications'));
    }

    public function showVersions()
    {
        $Versions = Version::all();
        return view('admin.publications.show_versions',compact('Versions'));
    }

    public function publicationsByVersion($id_version)
    {
        set_time_limit(0);
        $Version = Version::find($id_version);  

        $AllPublications = Publication::where('realtrim',$Version->RealTrim)->get();

        $Publications = array();
        foreach ($AllPublications as $Publication) {
            $int = intval($Publication->last_history[0]->ml_average);
            if($int<0){
                array_push($Publications, $Publication);
            }
        }
        $Groupname = $Version->name;
        return view('admin.publications.index',compact('Publications','Groupname'));
    }

    public function search()
    {
        $Marca = Attribute::all()->where('value_id','BRAND')->last();
        $Marcas = Attribute_Publication::distinct()->select('value_name')->where('attribute_id', $Marca->id)->groupBy('value_name')->get()->toarray();
        return view('admin.publications.search',compact('Marcas'));
    }

    public function results(Request $request)
    {
        set_time_limit(0);
        $PublicationsIds = Attribute_Publication::all()->where('value_name',$request->model);

  
        $Publications = array();
        foreach ($PublicationsIds as $Publication) {
            $Publication = Publication::find($Publication->publication_id);
            array_push($Publications, $Publication);
        }
        return view('admin.publications.index',compact('Publications'));
    } 

    public function searchPublications(Request $request)
    {
        $cantidad = 100;
        $offset=$request->offset;
        $cantidad = round((float)$cantidad/50);
        for ($i=1; $i < $cantidad; $i++) {
           $newoffset=(($i*50)-50)+$offset;
           $url = 'https://api.mercadolibre.com/sites/MCO/search?category='.$request['brand'].'&offset='.$newoffset.'&limit=50';
           $contents = file_get_contents($url);
           $array_hook = json_decode($contents, true);
           $resultados = $array_hook['results'];
           $this->storePublications($resultados);
        }
        $paging = $array_hook['paging'];
        $category = $array_hook['filters'];
        $result = array();
        array_push($result,$category[0]['values'][0]['id']);
        array_push($result,$paging); 
        return $result;
    }
  
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.publications.create'); 
    }

    public function newCreate()
    {
        return view('admin.publications.newcreate'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        set_time_limit(0);
        if($request->recursiva=='0'){
            $url = 'https://api.mercadolibre.com/sites/MCO/search?category='.$request->marca.'&offset='.$request->offset.'&limit=50';
            $contents = file_get_contents($url);
            $array_hook = json_decode($contents, true);
            $resultados = $array_hook['results'];
            $this->storePublications($resultados);
        }else{ 
            if($request->allbrands=='0'){

                $cantidad = $request->cant;
                $cantidad = round((float)$cantidad/50);

                for ($i=1; $i < $cantidad; $i++) { 
                    $offset=(($i*50)-50)+$request->offset;
                    $url = 'https://api.mercadolibre.com/sites/MCO/search?category='.$request->marca.'&offset='.$offset.'&limit=50';
                    $contents = file_get_contents($url);
                    $array_hook = json_decode($contents, true);
                    $resultados = $array_hook['results'];
                    $this->storePublications($resultados);
                }
            }else{
                $allBrands = array('MCO5783','MCO3185','MCO5779','MCO6672','MCO3174','MCO3180','MCO5739','MCO5683','MCO6600','MCO7079','MCO8125','MCO10357','MCO5681','MCO6038','MCO8480','MCO5743','MCO6173','MCO5748','MCO181120','MCO3205','MCO6041','MCO6109','MCO11927','MCO7078','MCO6583','MCO5753','MCO3196','MCO7080');

                foreach ($allBrands as $brand) {
                    $cantidad = 1000;
                    $cantidad = round((float)$cantidad/50);

                    for ($i=1; $i < $cantidad; $i++) { 
                        $offset=(($i*50)-50)+$request->offset;
                        $url = 'https://api.mercadolibre.com/sites/MCO/search?category='.$brand.'&offset='.$offset.'&limit=50';
                        $contents = file_get_contents($url);
                        $array_hook = json_decode($contents, true);
                        $resultados = $array_hook['results'];
                        $this->storePublications($resultados);
                    } 
                } 
            } 
        }//RECURSIVA

        return redirect()->route('publications.index')->with('message', 'Publicaciones guardadas exitosamente');
    }

    public function storePublications($resultados){
        set_time_limit(0);
        foreach ($resultados as $pub) {
            $atributos = array();
            $band = Publication::all()->where('idML',$pub['id']);
            if ($band->isEmpty()) {

                $data = Publication::create([
                    'title' => $pub['title'],
                    'idML' => $pub['id'],
                    'price' => $pub['price'],
                    'listing_type_id' => $pub['listing_type_id'],
                    'stop_time' => date_format(date_create($pub['stop_time']), 'Y/m/d'),
                    'condition' => $pub['condition'],
                    'permalink' => $pub['permalink'],
                    'thumbnail' => $pub['thumbnail'],
                    'state_name' => $pub['address']['state_name'],
                    'city_name' => $pub['address']['city_name'],
                    'original_price' => $pub['original_price'],
                    'available_quantity' => $pub['available_quantity'],
                    'sold_quantity' => $pub['sold_quantity'],
                    'buying_mode' => $pub['buying_mode'],
                ]); 

                // $id_publicacion = $data->id;


                foreach ($pub['attributes'] as $att) {
                    $band = Attribute::all()->where('value_id',$att['id'])->first();
                    if ($band == null) {

                        $datatt = Attribute::create([
                            'value_id' =>$att['id'],
                            'name' => $att['name'],
                        ]);

                        $id_atributo = $datatt->id;
                    }else{
                        
                        $id_atributo = $band->id;
                    }

                    $data->attributes()->attach([
                        $id_atributo => ['value_name' => $att['value_name']],
                    ]);
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Publication  $publication
     * @return \Illuminate\Http\Response
     */
    public function show(Request $id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Publication  $publication
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Publication = Publication::find($id);
        
        return view('admin.publications.show',compact('Publication'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Publication  $publication
     * @return \Illuminate\Http\Response
     */
    public function searchByBrand($brand)
    {
        ini_set('memory_limit','256M');
        $PublicationsIds = Attribute_Publication::all()->where('value_name',$brand);
        $Models = array();
        foreach ($PublicationsIds as $Publication) {
            $Publication = Publication::find($Publication->publication_id);
            if(isset($Publication->RealModel) && $Publication->RealModel!=null){
                array_push($Models, $Publication->RealModel);
            }
        } 
        $Models = array_unique($Models);
        sort($Models);
        return json_encode($Models);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Publication  $publication
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Publication = Publication::find($id);
        $realtrim = $Publication->realtrim;
        $Publication->delete();
        $porciones = explode("-", $realtrim,2);
        $Version = Version::all()->where('name',$porciones[1])->first();
        $cont = tagPublications($Version->id);

        return redirect()->route('publications.index')->with('message','Publicación borrada y nuevos promedios calculados con '.$cont.' registros');
    }

    public function sold($id)
    {
        $Publication = Publication::find($id);
        $now = \Carbon\Carbon::now();
        $Publication->sold_date = $now;
        $Publication->sold = 1;
        $Publication->save();  
        return redirect()->route('publications.index')->with('message','Publicación marcada como vendida');
    }

    public function validator($request,$id){
        $passwordvalidate = ($request['password']!=null) ? 'min:8' : '';
        $data = request()->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => $passwordvalidate,
        ], [
            'name.required' => 'El campo nombre es obligatorio',
            'email.required' => 'El campo email es obligatorio',
            'email.email' => 'Por favor ingresa una dirección valida',
            'email.unique' => 'Ya existe un usuario con ese email',
            'password.min' => 'Tu Password es muy corto',
        ]);
    }
}
