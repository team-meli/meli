<?php

namespace App\Http\Controllers;

use App\Version;
use App\Subversion;
use App\Attribute;
use App\Attribute_Publication;
use App\Publication;
use App\Average;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class VersionController extends Controller
{
    public function index()
    {
        $Versions = Version::all();
        return view('admin.versions.index',compact('Versions'));
    }

    public function getMoreattr($model_id,$version_id)
    {
        $Pubs =  Publication::all()->where('model_id',$model_id)->take(50);
        foreach ($Pubs as $Pub) {
            $val = moreAttr($Pub->id_ML);
            $Pub->AIRE = $val;
            $Pub->save();
        }

        $Version = Version::find($version_id); 
        return view('admin.versions.edit',compact('Version'));
    }

    public function viewAll($brand_id=false)
    {
        if (!$brand_id) {

            $Brands = [];//name,cantPub,cantVersions

            $brands_id = Version::select('brand_id')->distinct()->get();
             
            foreach ($brands_id as $brand_id) {
                $Brand=[];
                $Pubs = Publication::all()->where('brand_id',$brand_id->brand_id);
                $Brand['id']=$brand_id->brand_id;
                $Brand['cantPubs'] = $Pubs->count();
                if($Pubs->count()>0){
                    $Brand['name'] = $Pubs->first()->BRAND;
                }else{
                    $Brand['name'] = $brand_id->brand_id;
                }
                $Brand['cantVersions'] = Version::all()->where('brand_id',$brand_id->brand_id)->count(); 
                array_push($Brands, $Brand);
            }
            return view('admin.versions.index',compact('Brands'));
        }else{
            $Versions = Version::all()->where('brand_id',$brand_id);
            return view('admin.versions.index',compact('Versions'));
        }   
    }

    public function getVersionsByModel($model)
    {
        $Versions = Version::all()->where('model_id',$model);
        return response()->json(['status'=>true,'data'=>$Versions]);
    }

    public function create()
    { 
        return view('admin.versions.create' );
    }

    public function store(Request $request)
    {
        $Version = Version::create([
            'name' => strtoupper(request('name')),
            'modelo' => request('model_name'), 
            'model_id' => request('model_code'),
            'brand_id' => request('brand'),  
        ]);
        // return redirect('/dash/versions/create')->with('info', 'La version fue creada con exito');
        return redirect('/dash/versions/'.$Version->id.'/edit')->with('info', 'La version fue creada con exito');
    }

    public function edit($id)
    {  
        $Version = Version::find($id); 
        return view('admin.versions.edit',compact('Version'));
    }

    public function update(Request $request, $id)
    {
 
        $Version = Version::find($id);
        $oldname = $Version->name;
        $oldmodelo = $Version->modelo;
        $Version->name = strtoupper($request['name']);
        $Version->modelo = ($request['model_name']);
        $OldRealVersion = $oldmodelo.'-'.$oldname;
        $NewRealVersion = $Version->modelo.'-'.$Version->name;
        $Version->save();

        // if($oldmodelo!=$request['modelo']){
        //     $Averages = Average::where('trim','=',$OldRealVersion)->delete();
        //     $Subversions = Subversion::where('version_id','=',$id)->delete();
        // }

        // $MassUpdate =  Publication::where('realtrim','=',$OldRealVersion)->update(['realtrim' => $NewRealVersion]); 
        return redirect('/dash/versions/'.$id.'/edit')->with('info', 'Versión editada correctamente');
    }    
    public function searchByModel($model)
    {
        $Versions = Version::All()->where('modelo',($model));
        return json_encode($Versions);
    }

    public function searchSubversions($version,$model)
    {
        ini_set('memory_limit','256M');
        $Publications = Publication::all()->where('model_id',$model);
        $Version = Version::find($version); 
        $AnotherSubVersions = Subversion::all()->where('model_id',$model); //PARA OBTENER LAS YA TAGGEADAS Y OCULTARLAS   
        $SuperVersions = array();
        
        foreach ($Publications as $Publication) { 
            if(isset($Publication->RealVersion) && $Publication->RealVersion!=null){
                $SupVer = array();
                (isset($Publication->ENGINE_DISPLACEMENT))? $cilindraje=$Publication->ENGINE_DISPLACEMENT : $cilindraje='--';
                $SupVer['realversion'] = $Publication->RealVersion;
                $SupVer['transmission'] = $Publication->TRANSMISSION;
                $SupVer['doors'] = $Publication->DOORS;
                $SupVer['cilindraje'] = $cilindraje;
                $SupVer['link'] = $Publication->permalink;
                $SupVer['combustible'] = $Publication->FUEL_TYPE;
                $SupVer['aire'] =($Publication->AIRE != '')?  $Publication->AIRE : '--';
                $SupVer['checked'] = '';
                $SupVer['id_subversion'] = '0';  
                foreach ($Version->subversions as $subversion) {
                    if($Publication->RealVersion==$subversion->name
                        && $Publication->TRANSMISSION==$subversion->transmission
                        && $Publication->DOORS==$subversion->doors){
                        $SupVer['checked'] = 'checked';
                        $SupVer['id_subversion'] = $subversion->id;
                    }
                }    
                array_push($SuperVersions, $SupVer);  
            }
        }

        // Organiza el array de subversiones
        usort($SuperVersions, function ($a, $b) {
            return $a['realversion'] <=> $b['realversion'];
        });
        $serialized = array_map('serialize', $SuperVersions);
        $unique = array_unique($serialized);
        $SuperVersions = array_intersect_key($SuperVersions, $unique);

        // Elimina las que ya esten en otra version
        foreach ($SuperVersions as $key => $SupVer){ 
            foreach ($AnotherSubVersions as $subversion){
              if($subversion->version_id!=$version)
                if($SupVer['realversion']==$subversion->name
                    && $SupVer['transmission']==$subversion->transmission
                    && $SupVer['doors']==$subversion->doors){ 
                    unset($SuperVersions[$key]);
                } 
            } 
        }   
        return json_encode($SuperVersions);
    }

    public function searchSubversionsHT($model)
    {
        ini_set('memory_limit','256M');
     
        $AnotherSubVersions = Subversion::all()->where('modelo',$model); //PARA OBTENER LAS YA TAGGEADAS Y OCULTARLAS

        $Model = Attribute::all()->where('value_id','MODEL')->last();
        $PublicationsIds = Attribute_Publication::all()->where('attribute_id',$Model->id)->where('value_name',$model);
        $Versions = array();
        $SuperVersions = array();
        foreach ($PublicationsIds as $Publication) {
            $Publication = Publication::find($Publication->publication_id);
            if(isset($Publication->RealVersion) && $Publication->RealVersion!=null){
                $SupVer = array();
                $SupVer['realversion'] = $Publication->RealVersion;
                $SupVer['transmission'] = $Publication->RealTransmission;
                $SupVer['doors'] = $Publication->RealDoors;
                $SupVer['checked'] = '';
                $SupVer['id_subversion'] = '0';
                array_push($SuperVersions, $SupVer);
                array_push($Versions, $Publication->RealVersion);
            }
        }
        usort($SuperVersions, function ($a, $b) {
            return $a['realversion'] <=> $b['realversion'];
        });

        $serialized = array_map('serialize', $SuperVersions);
        $unique = array_unique($serialized);
        $SuperVersions = array_intersect_key($SuperVersions, $unique);

       foreach ($SuperVersions as $key => $SupVer)  { 
            foreach ($AnotherSubVersions as $subversion) {
                if($SupVer['realversion']==$subversion->name
                    && $SupVer['transmission']==$subversion->transmission
                    && $SupVer['doors']==$subversion->doors){
                    unset($SuperVersions[$key]);
                } 
            } 
        }   
        return json_encode($SuperVersions);
    }

    public function destroy($id)
    {
        $should_delete = true;
        $Version = Version::find($id);
        $message = 'La version fue eliminada correctamente';

        $AModel = $Version->modelo;
        $AVersion = $Version->name;
        $RealVersion = $AModel.'-'.$AVersion;

        $MassUpdate =  Publication::where('realtrim','=',$RealVersion)->update(['realtrim' => null]); 
        $Publications = Publication::all()->where('realtrim','=',$RealVersion); 
        $Averages = Average::where('realtrim','=',$RealVersion)->delete();
        $Subversions = Subversion::where('version_id','=',$id)->delete();    

        if ($Publications->isNotEmpty()) {
            $should_delete = false;
            $message = 'La versión tiene publicaciones taggeadas, no se puede eliminar';
        }

        if ($should_delete == true) {
            $Version->delete();
        }

        return redirect()->route('versions.index')->with('message', $message); 
    }

    public function validator($request,$id)
    {
        // $data = request()->validate([
        //     'name' => 'required|unique:versions,name,'.$id.',NULL,id,modelo,'.$request('model'),
        // ], [
        //     'name.required' => 'El nombre es obligatorio',
        //     'name.unique' => 'Ya existe una version con ese nombre'
        // ]);


        $name = request('name');
        $modelo = request('model');
        $data = request()->validate([
            'name' => [
                'required',
                Rule::unique('versions')->where(function ($query) use($name,$modelo) {
                    return $query->where('name', $name)
                    ->where('modelo', $modelo);
                }),
            ],
        ], [
            'name.required' => 'El nombre es obligatorio',
            'name.unique' => 'Ya existe una version con ese nombre asignada a ese modelo'
        ]);
    }

}
