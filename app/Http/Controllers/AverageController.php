<?php

namespace App\Http\Controllers;

use App\Average;
use App\Attribute;
use App\Version;
use App\Publication;
use App\Range;
use App\History;
use App\Attribute_Publication;
use Illuminate\Http\Request;

class AverageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Averages = Average::all();
        return view('admin.averages.index', compact('Averages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $Marca = Attribute::all()->where('value_id','BRAND')->last();
        $Brands = Attribute_Publication::distinct()->select('value_name')->where('attribute_id', $Marca->id)->groupBy('value_name')->get()->toarray();

        return view('admin.averages.create', compact('Brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $AModel = request('model');
        $AVersion = request('version');
        $AVersion = Version::find($AVersion);
        $AVersion = $AVersion->name;
        $RealVersion = $AModel.'-'.$AVersion;
        

        $Publications = Publication::all()->where('realtrim','=',$RealVersion);
        $promedio_motor = request('promedio_motor');
        $model = request('model');
        $year = request('year');
        $version = request('version');
        $RealVersion = $RealVersion;
        $createAverage = $this->createAverage($Publications,$promedio_motor,$model,$year,$version,$RealVersion);


        return redirect()->route('averages.index')->with('message', 'El promedio fue calculado');
    }



    public function tagPublications($version_id)
    {
        ini_set('memory_limit','256M');
        $cont = tagPublications($version_id);
        return redirect('/dash/versions/'.$version_id.'/edit')->with('info', 'Se han taggeado '.$cont.' publicacion(es)');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Promedio  $promedio
     * @return \Illuminate\Http\Response
     */
    public function show(Promedio $promedio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Promedio  $promedio
     * @return \Illuminate\Http\Response
     */
    public function edit(Promedio $promedio)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Promedio  $promedio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Promedio $promedio)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Promedio  $promedio
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Average = Average::find($id);
        $Average->delete();
        return redirect()->route('averages.index')->with('message','El promedio de borro con exito.');
    }
}
