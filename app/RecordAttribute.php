<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecordAttribute extends Model
{
    protected $fillable = [
    	'id_ML','attribute','value'
    ]; 
}
