<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subversion extends Model
{
    protected $fillable = [
    	'name','version_id','model_id','doors','transmission'
    ];
    public function version()
    {
        return $this->hasOne('App\Version', 'id', 'version_id');
    }
}
