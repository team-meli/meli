<?php

namespace App;
use App\Average;
use App\Range;
use Illuminate\Database\Eloquent\Model;

class Publication extends Model
{
	protected $fillable = [
        'title',
        'id_ML',
        'price',
        'brand_id',
        'model_id',
        'realtrim', 
        'listing_type_id',
        'stop_time',
        'permalink',
        'thumbnail',
        'state_name',
        'city_name',
        'original_price',
        'available_quantity',
        'sold_quantity',
        'buying_mode', 
        'BRAND',
        'MODEL',
        'DOORS',
        'FUEL_TYPE',
        'KILOMETERS',
        'TRANSMISSION',
        'TRIM',
        'VEHICLE_YEAR',
        'ENGINE_DISPLACEMENT',
        'TRACTION_CONTROL',
        'ITEM_CONDITION',
        'sold',
        'sold_date',
        'AIRE',
        'BAROMETER'
    ];

    public function attributes()
    {
        return $this->belongsToMany(Attribute::class, 'attribute_publication')->withPivot(["value_name"]);
    }
    public function getRealPriceAttribute()
    {
        $price = ($this->price);
        $price = round($price,2);

    	$realprice = '$'.number_format ( $price, 0 , "," , "." );
        return $realprice;
    }
    public function getRealBrandAttribute()
    {
    	$attribute = $this->attributes()->where('value_id','BRAND')->get();
        if(isset($attribute[0]) && $attribute[0]!=null)
    	   return $attribute[0]['pivot']['value_name'];
    }
    public function getRealModelAttribute()
    {
    	$attribute = $this->attributes()->where('value_id','MODEL')->get();
        if(isset($attribute[0]) && $attribute[0]!=null)
    	   return $attribute[0]['pivot']['value_name'];
    }
    public function getRealYearAttribute()
    {
    	$attribute = $this->attributes()->where('value_id','VEHICLE_YEAR')->get();
        if(isset($attribute[0]) && $attribute[0]!=null)
    	   return $attribute[0]['pivot']['value_name'];
    }
    public function getRealKMAttribute()
    {
        $attribute = $this->KILOMETERS;
        if(isset($attribute[0]) && $attribute[0]!=null){
            $realkm = str_replace(' km', "", $attribute);
            return $realkm;
        }
    }
    public function getRealTransmissionAttribute()
    {
        $attribute = $this->attributes()->where('value_id','TRANSMISSION')->get();
        if(isset($attribute[0]) && $attribute[0]!=null){
            return $attribute[0]['pivot']['value_name'];
        }
    }
    public function getRealDoorsAttribute()
    {
        $attribute = $this->attributes()->where('value_id','DOORS')->get();
        if(isset($attribute[0]) && $attribute[0]!=null){
            return $attribute[0]['pivot']['value_name'];
        }
    }

    public function getBarometroAttribute($params=false)
    {    
        $ptBarometro=0;
        if (!$params) {
            $params = [
                'ml_average' => 60,
                'ml_average_km' => 20,
                'average_expected_km' => 20,
            ];  
        }  
        foreach ($params as $key => $peso) {   
            $value = $this->histories->last()->$key; 
            $avarageBar = (($peso*abs($value))/100);
            $ptBarometro += $avarageBar;  
        }  
        return $ptBarometro;
    }

    public function getJustTrimAttribute()
    {
        $realtrim = $this->realtrim;
        $JustTrim = explode('-', $realtrim, 2);
        if(isset($JustTrim[1])){
            return $JustTrim[1];
        }else{
            return '--';
        }
        
    }
    public function getRealVersionAttribute()
    { 
        if(isset($this->TRIM) && $this->TRIM!=null){
            $realversion = strtoupper($this->TRIM);
            // $realversion = str_replace(strtoupper($this->RealModel).' ', ".", $realversion);
            // $realversion = str_replace(strtoupper($this->RealYear).' ', ".", $realversion);
            // $realversion = str_replace(strtoupper($this->RealBrand).' ', ".", $realversion);
            // $realversion = str_replace(strtoupper(' '.$this->RealModel), "/", $realversion);
            // $realversion = str_replace(strtoupper(' '.$this->RealYear), "/", $realversion);
            // $realversion = str_replace(strtoupper(' '.$this->RealBrand), "/", $realversion);
            $realversion = str_replace(strtoupper('ALL'), ".", $realversion);
            $realversion = str_replace(strtoupper('NEW'), ".", $realversion);
            $realversion = str_replace(strtoupper('VIDRIOS'), ".", $realversion);
            $realversion = str_replace(strtoupper('ELECTRIC'), ".", $realversion);
            $realversion = str_replace(strtoupper('ELECTRICO'), ".", $realversion);
            $realversion = str_replace(strtoupper('EQUIPO'), ".", $realversion);
            $realversion = str_replace(strtoupper('FULL'), ".", $realversion);
            $realversion = str_replace(strtoupper('SUN'), ".", $realversion);
            $realversion = str_replace(strtoupper('ROOF'), ".", $realversion);
            $realversion = str_replace(strtoupper('VERSION'), ".", $realversion);
            $realversion = str_replace(strtoupper('CC'), "", $realversion);
            $realversion = str_replace(',', "", $realversion);
            $realversion = str_replace("/", "", $realversion);
            $realversion = str_replace(" .", "", $realversion);
            $realversion = str_replace(". ", "", $realversion);
            $realversion = str_replace(".", "", $realversion);
            $realversion = str_replace("-", "", $realversion);
            $realversion = str_replace(" ", "-", $realversion);
            $realversion = str_replace("---", "-", $realversion);
            $realversion = str_replace("--", "-", $realversion);
           return $realversion;
        }
    }
    public function getFullBrandModelAttribute()
    {
        return $this->RealBrand.' - '.$this->RealModel.' - '.$this->RealYear;
    }

    public function histories() {
        return $this->hasMany('App\History');
    }

    public function last_history() {
        return $this->hasMany('App\History')->limit(1);
    }

    public function getRealRangeAttribute() {
        $return = $this->histories->last()->ml_average_km_tooltip;
        $return = str_replace("<em>", "", $return);
        $return = str_replace("</em>", "", $return);
        $return = str_replace("milKM","", $return);
        $return = explode("<br>", $return);
        $return = explode("~", $return[0]);
        $return = $return[0];
        // $return = str_replace("N/A", "99999", $return); 
        // if($return[0]=='0 - 10'){
        //     return "";
        // }
        return $return; 
    }

    //AVERAGES 

    // public function getMlAverageAttribute()
    // {
    //     $average = Average::all()->where('realtrim', $this->realtrim)->where('year',$this->RealYear)->last();
    //     $return = 'N/A';
    //     if ($average == null || $average->promedio_ml==0) {
    //         $return = 'N/A';
    //     }else{
    //             $average=$average->promedio_ml;
    //             $price =  $this->price;
    //             $return = ((round(($price*100)/$average))-100).'%';
    //     }

    //     return  $return;
    // }
    // public function getMlAverageTooltipAttribute()
    // {
    //     $average = Average::all()->where('realtrim', $this->realtrim)->where('year',$this->RealYear)->last();
    //     $tooltip = 'N/A';
    //     if ($average == null || $average->promedio_ml==0) {
    //         $tooltip = 'N/A';
    //     }else{
    //         $qty=0;
    //         $average->promedio_ml = round(($average->promedio_ml),2);
    //         $tooltip = '$'.number_format ( $average->promedio_ml, 2 , "," , "." ).' ('.$average->qty_ml.')';
    //     }
    //     return  $tooltip;
    // }
    // public function getMlAverageKMAttribute()
    // {
    //     $rangekm=0;
    //     $return = 'N/A';
    //     $average = Average::all()->where('realtrim', $this->realtrim)->where('year',$this->RealYear)->last();
    //     if($average!=null){
    //         $Ranges=Range::all()->where('average_id',$average['id']);
    //         foreach ($Ranges as $Range) {
    //             if($this->RealKM > $Range->min && $this->RealKM < $Range->max){
    //                 $rangekm=$Range->average;
    //             }
    //         }
    //     }
    //     if ($rangekm!=0) {
    //         $averagekm=$rangekm;
    //         $price =  $this->price;
    //         $return = ((round(($price*100)/$averagekm))-100).'%';
    //     }
    //     return  $return;
    // }
    // public function getMlAverageKMTooltipAttribute()
    // {
    //     $rangekm=0;
    //     $rangeqty=0;
    //     $rangemin=0;
    //     $rangemax=0;
    //     $tooltip = 'N/A';
    //     $average = Average::all()->where('realtrim', $this->realtrim)->where('year',$this->RealYear)->last();
    //     if($average!=null){
    //         $Ranges=Range::all()->where('average_id',$average['id']);
    //         foreach ($Ranges as $Range) {
    //             if($this->RealKM > $Range->min && $this->RealKM < $Range->max){
    //                 $rangekm=$Range->average;
    //                 $rangeqty=$Range->qty;
    //                 $rangemin=round(($Range->min/1000),0);
    //                 $rangemax=round(($Range->max/1000),0);
    //             }
    //         }
    //     }
    //     if ($average == null || $rangekm==0) {
    //         $tooltip = 'N/A';
    //     }else{
    //             $qty=0;
    //             $rangekm = round(($rangekm),2);
    //             $tooltip = '<em>'.$rangemin.'milKM</em> ~ <em>'.$rangemax.'milKM</em> <br>$'.number_format ( $rangekm, 2 , "," , "." ).' ('.$rangeqty.')';
    //     }
    //     return  $tooltip;
    // }

    // public function getMotorAverageAttribute()
    // {

    //     $average = Average::all()->where('realtrim', $this->realtrim)->where('year',$this->RealYear)->last();
    //     $return = 'N/A';
    //     if ($average == null || $average->promedio_motor==0) {
    //         $return = 'N/A';
    //     }else{
    //             $average=$average->promedio_motor;
    //             $price =  $this->price;
    //             $return = ((round(($price*100)/$average))-100).'%';
    //     }

    //     return  $return;
    // }
    // public function getMotorAverageTooltipAttribute()
    // {
    //     $average = Average::all()->where('realtrim', $this->realtrim)->where('year',$this->RealYear)->last();
    //     $tooltip = 'N/A';
    //     if ($average == null || $average->promedio_motor==0) {
    //         $tooltip = 'N/A';
    //     }else{
    //         $qty=0;
    //         $average->promedio_motor = round(($average->promedio_motor),2);
    //         $tooltip = '$'.number_format ( $average->promedio_motor, 2 , "," , "." ).' ('.$average->qty_ml.')';
    //     }
    //     return  $tooltip;
    // }
    // public function getExpectedKMAttribute()
    // {
    //     $month = date('m');
    //     $currentyear = date('Y');
    //     $CurrentKM = (14000/12)*$month;
    //     $Age = (float)$currentyear-(float)($this->RealYear);
    //     $AgeKM = $Age*14000;
    //     $difAge = $Age-((float)$currentyear-2007);
    //     $difAgeKM=0;
    //     if($difAge>0){
    //         $difAgeKM=$difAge*6000;
    //     }
    //     $AgeKM=$AgeKM+$difAgeKM+$CurrentKM;
    //     return $AgeKM;
    // }
    // public function getAverageExpectedKMAttribute()
    // {
    //     $return = ((round(($this->RealKM*100)/$this->ExpectedKM))-100).'%';
    //     return $return;
    // }
    // public function getAverageExpectedKMTooltipAttribute()
    // {
    //     $tooltip = '<b>Tiene:</b> '.round(($this->RealKM/1000),1).'milKM <br> <b>De:</b> '.round(($this->ExpectedKM/1000),1).'milKM ';
    //     return  $tooltip;
    // }

    // public function getBarometroAttribute()
    // {
    //     $tipokm = '';
    //     $barokm = str_replace('%', "", $this->AverageExpectedKM);
    //     $barokm = (float)$barokm;
    //     $baro1 = str_replace('%', "", $this->MlAverage);
    //     $baro2 = str_replace('%', "", $this->MlAverageKM);
    //     $baro3 = str_replace('%', "", $this->MotorAverage);
    //     $barometro = (float)$baro1+(float)$baro2+(float)$baro3;
    //     $barometro = round($barometro/3);
    //     $barometro = $barometro.'%';
    //     if($baro1=="N/A" || $baro2=="N/A" || $baro3=="N/A" || $barokm=="N/A"){
    //         $barometro = 'N/A';
    //     }
    //     if($barokm>19){
    //         $tipokm = 'C';
    //     }
    //     if($barokm<20){
    //         $tipokm = 'B';
    //     }
    //     if($barokm<0){
    //         $tipokm = 'A';
    //     }
    //     if($barokm<(-50)){
    //         $tipokm = 'AA';
    //     }
        
        
    //     return  $barometro.' '.$tipokm;
    // }

}
