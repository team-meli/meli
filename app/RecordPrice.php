<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecordPrice extends Model
{
    protected $fillable = [
    	'id_ML','price'
    ]; 
}
