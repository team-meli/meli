<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
	protected $fillable = [ 
		'value_id',
		'name'
    ];

    public function publication()
    {
        return $this->belongsToMany(Publication::class, 'attribute_publication');
    }
}