<?php

namespace App\Console\Commands;
use App\Version;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class MorePublications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'publications:more';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Trae publicaciones de ML por modelo';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $Versions = Version::distinct('model_id')->get();
        $FinalVersions = array();
        foreach ($Versions as $Version) {  
            if ($Version->subversions->count()>=10) {
                array_push($FinalVersions, $Version->model_id);
            } 
        }
        $FinalVersions = array_unique($FinalVersions,SORT_STRING);
        
        foreach ($FinalVersions as $Version) { 
            $publications = getAllPub($Version); 
            storeUpdatePublications($Version,$publications);
            Log::info('Publicaciones actualizadas del modelo '.$Version);
        }
    }
}
