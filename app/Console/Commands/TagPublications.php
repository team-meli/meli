<?php

namespace App\Console\Commands;
use App\Version;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class TagPublications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'publications:tag';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Taggea publicaciones de ML por version';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $Versions = Version::all();
        foreach ($Versions as $Version) { 
            $cont = tagPublications($Version->id);
            Log::info('Se han taggeado '.$cont.' de la version '.$Version->name);
        }
    }
}
