<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Publication;

class Version extends Model
{
    protected $fillable = [
    	'name','modelo','model_id','brand_id'
    ];
    public function subversions()
    {
        return $this->hasMany('App\Subversion', 'version_id', 'id');
    }
    public function getRealTrimAttribute()
    {
        return $this->model_id.'-'.$this->name; 
    }
    // public function publications(){
    
    //     return $this->hasMany('App\Publication', 'realtrim', $this->RealTrim);
    // }
    public function getBrandAttribute()
    {
        $brand = Publication::where('brand_id',$this->brand_id)->get()->first(); 
        (!isset($brand))? $brand = 'No disponible' : $brand=$brand->BRAND;
        return $brand; 
    }
}
