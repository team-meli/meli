<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Average extends Model
{
    protected $fillable = [
        'promedio_ml',
        'promedio_motor',
        'model_id',
        'year',
        'trim',
        'qty_ml',
    ];
    public function ranges()
    {
        return $this->hasOne('App\Range', 'average_id', 'id');
    }
    public function version()
    {
        return $this->hasOne('App\Version', 'id', 'version_id');
    }
    public function getRealPromedioMLAttribute()
    {
        $real = ($this->promedio_ml);
        $real = round($real,2);
        $return = '$'.number_format ( $real, 2 , "," , "." );
        return $return;
    }
    public function getRealPromedioMotorAttribute()
    {
        $real = ($this->promedio_motor);
        $real = round($real,2);
        $return = '$'.number_format ( $real, 2 , "," , "." );
        return $return;
    }
}
