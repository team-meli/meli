<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
	protected $fillable = [ 
        'publication_id',
        'price',
        'ml_average',
        'ml_average_tooltip',
        'ml_average_km',
        'ml_average_km_tooltip',
        'motor_average',
        'motor_average_tooltip',
        'expected_km',
        'average_expected_km',
        'average_expected_km_tooltip',
    ];

     public function getRealPriceAttribute()
    {
        $price = ($this->price);
        $price = round($price,2);

    	$realprice = '$'.number_format ( $price, 0 , "," , "." );
        return $realprice;
    }
}
