<?php  
use App\Publication;
use App\RecordPrice;
use App\RecordAttribute;
use App\Version;
use App\Average;
use App\Range;
use App\History;
use Illuminate\Support\Collection;

set_time_limit(0);

function moreAttr($id_ML){
    $pub = getMoreAttr($id_ML);
    $aire = 'N/A';
    foreach ($pub['attributes'] as $attr) {
        ($attr['id']=='HAS_AIR_CONDITIONING')? $aire=$attr['value_name'] : null;
    } 
    return $aire;
}

function bestSeller($check=false)
{
    //Marcas dentro de la categoria carros y camionetas Colombia
    $models_best_seller=[]; 
    $brands_catalog = getCat('MCO1744');  
    $cont = 0;
    foreach ($brands_catalog['children_categories'] as $brand_catalog) { 
        echo $brand_catalog['name'];
        echo "\n---------------------------\n";
        $models_catalog = getCat($brand_catalog['id']); 
        foreach ($models_catalog['children_categories'] as $model_catalog) { 
            echo $model_catalog['name'];
            echo "\n";
            $model = getCat($model_catalog['id']);
            if (!$check) { 
                array_push($models_best_seller,[
                    'marca' => $brand_catalog['name'],
                    'modelo'=> $model['name'], 
                    'cant'  => $model['total_items_in_this_category']
                ]);
            }
        } 
        echo "---------------------------\n";
        echo "---------------------------\n\n"; 
    }
    usort($models_best_seller, "cmp");
    dd($models_best_seller);
}

function cmp($a, $b) { return $b['cant'] - $a['cant']; }

function getAllPub($category){  
	$offset=0;
	$publications=[];
	do {
		$query = searchByCat($category,$offset); 
		$publications = array_merge($publications, $query['results']); 
		$offset+= 50; 
	} while ($offset < 1000 && $offset < (int)$query['paging']['total']); 
	return (count($publications))? $publications:false;
}

function storeUpdatePublications($brand_code,$publications){ 
	$data = [];
    $localPublications = Publication::all();  
    foreach ($publications as $pub) {  
        $localPub = $localPublications->firstWhere('id_ML',$pub['id']);
        if ($localPub) {
            $attr = getAttrPublucation($pub);
        	if ($localPub->price != $pub['price']) {
        		RecordPrice::create([
        			'id_ML' => $localPub->id_ML,
        			'price' => $pub['price']
        		]);
                $localPub->price = $pub['price'];
        	}
            if($localPub->KILOMETERS != $attr['KILOMETERS']){
                RecordAttribute::create([
                    'id_ML' => $localPub->id_ML,
                    'attribute' => 'KILOMETERS',
                    'value' => $attr['KILOMETERS']
                ]);
                $localPub->KILOMETERS = $attr['KILOMETERS'];
            }

            $localPub->save();
        }else{
            if ($pub['price'] > 5000000 && $pub['price'] < 1000000000) {
                $row = createRowPub($brand_code,$pub); 
                array_push($data, $row); 
            } 
        }
    }
    $result = Publication::insert($data);
    return $result;
}

function createRowPub($brand_code,$pub){
	$attr = getAttrPublucation($pub); 
    $row = [
    	'title' => $pub['title'],
    	'id_ML' => $pub['id'],
    	'price' => $pub['price'],
        'brand_id' => $brand_code,
        'model_id' => $pub['category_id'], 
        'realtrim' => '',
    	'listing_type_id' => $pub['listing_type_id'],
    	'stop_time' => date_format(date_create($pub['stop_time']), 'Y/m/d'), 
    	'permalink' => $pub['permalink'],
    	'thumbnail' => $pub['thumbnail'],
    	'state_name' => $pub['address']['state_name'],
    	'city_name' => $pub['address']['city_name'],
    	'original_price' => $pub['original_price'],
    	'available_quantity' => $pub['available_quantity'],
    	'sold_quantity' => $pub['sold_quantity'],
    	'buying_mode' => $pub['buying_mode'],
        'BRAND' => $attr['BRAND'],
        'MODEL' => $attr['MODEL'],
        'DOORS' => $attr['DOORS'],
        'FUEL_TYPE' => $attr['FUEL_TYPE'],
        'KILOMETERS' => $attr['KILOMETERS'],
        'TRANSMISSION' => $attr['TRANSMISSION'],
        'TRIM' => $attr['TRIM'],
        'VEHICLE_YEAR' => $attr['VEHICLE_YEAR'],
        'ENGINE_DISPLACEMENT' => $attr['ENGINE_DISPLACEMENT'],
        'TRACTION_CONTROL' => $attr['TRACTION_CONTROL'],
        'ITEM_CONDITION' => $pub['attributes'][0]['value_name'],
    ];
    return $row;
}

function getAttrPublucation($pub){	
	$pubAttr = $pub['attributes']; 
	$attr = [
		'BRAND' => '',
        'MODEL' => '',
		'DOORS' => '',
		'FUEL_TYPE' => '',
		'KILOMETERS' => '',
		'TRANSMISSION' => '',
		'TRIM' => '',
		'VEHICLE_YEAR' => '',
		'ENGINE_DISPLACEMENT' => '',
		'TRACTION_CONTROL' => ''
	]; 
	foreach ($attr as $name => $value) {
	    $key = array_search($name, array_column($pubAttr, 'id'));
	    ($key)? $attr[$name]=$pubAttr[$key]['value_name'] : $attr[$name]=null; 
	}  
	return $attr;
}

function getBrandModels($cat='MCO1744') { 
	$mainCat = getCat($cat); 
	$arrayBrands = $mainCat['children_categories']; 
	$arrayModels = [];  
	foreach ($arrayBrands as $brand) {    
		$brand = getCat($brand['id']); 
		$models = $brand['children_categories'];
		array_push($arrayModels, $models); 
	} 
	dd($arrayBrands,$arrayModels); 
} 

function hasPub($cat){
    $flag = Publication::where('brand_id',$cat)->orWhere('model_id',$cat)->get()->count(); 
    return $flag;
}

function searchByCat($category,$offset=0,$limit=50){
    $url = 'https://api.mercadolibre.com/sites/MCO/search?category='.$category.'&offset='.$offset.'&limit='.$limit;
    $contents = file_get_contents($url);
    $array_hook = json_decode($contents, true);
    return $array_hook;
} 

function getCat($codeCat){
    $url = 'https://api.mercadolibre.com/categories/'.$codeCat;
    $contents = file_get_contents($url);
    $array_hook = json_decode($contents, true);
    return $array_hook;
} 

function getMoreAttr($id_ML){
    $url = 'https://api.mercadolibre.com/items/'.$id_ML;
    $contents = file_get_contents($url);
    $array_hook = json_decode($contents, true);
    return $array_hook;
} 


function tagPublications($version_id) {
    ini_set('memory_limit','256M');
    $Version = Version::find($version_id);
    // $Model = $Version->modelo;
    $subversions = array();
    array_push($subversions, $Version->name);
    foreach ($Version->subversions as $subversion) {
        array_push($subversions, $subversion->name);
    }
    $cont = 0;
    // $Publications = Attribute_Publication::all()->where('value_name','=',$Model)->toarray();
    $Publications = Publication::all()->where('model_id', $Version->model_id);
    foreach ($Publications as $r) { 
        foreach ($subversions as $subversion) {
            if( strpos($r->RealVersion, $subversion) !== false){
                $r->realtrim = $Version->model_id.'-'.$Version->name; 
                $r->save();
                $cont++;
            }
        }
    }
    $RealVersion = $Version->model_id.'-'.$Version->name;
    $cont = Publication::all()->where('realtrim','=',$RealVersion)->count();


    $createAllYearsAverages = createAllYearsAverages($RealVersion,$Version->model_id,$Version->id);

    $createHistories = createHistories($RealVersion);

    return $cont;
    
}

function createAllYearsAverages($RealVersion,$model,$version){
    $years =  Publication::distinct()->select('VEHICLE_YEAR')->where('realtrim','=',$RealVersion)->groupBy('VEHICLE_YEAR')->get()->toarray();
    $Publications = Publication::all()->where('realtrim','=',$RealVersion);

    foreach ($years as $year) {
       $createAverage = createAverage($Publications,0,$model,$year['VEHICLE_YEAR'],$version,$RealVersion);
    }

    return '';
}


function createHistories($RealVersion){
    $Publications = Publication::all()->where('realtrim','=',$RealVersion);
    foreach ($Publications as $r) {
        $average = Average::all()->where('trim', $r->realtrim)->where('year',$r->VEHICLE_YEAR)->last();
        $MlAverage = 'N/A';
        $MlAverageTooltip = 'N/A';
        $MlAverageKM = 'N/A';
        $MlAverageKMTooltip = 'N/A';
        $MotorAverage = 'N/A';
        $MotorAverageTooltip = 'N/A';
        $ExpectedKM = 'N/A';
        $AverageExpectedKM = 'N/A';
        $AverageExpectedKMTooltip = 'N/A';
        $Barometro = 'N/A';
        $price =  $r->price;
        
        if ($average != null){ 
            if($average->promedio_ml!=0){
                $average_ml=$average->promedio_ml;
                $MlAverage = ((round(($price*100)/$average_ml))-100);
                $MlAverageTooltip = '$'.number_format( $average_ml, 0 , "," , "." ).' ('.$average->qty_ml.')';
            } // QUE EL PROMEDIO ML NO ESTE VACIO

            if($average->promedio_motor!=0){
                $average_motor=$average->promedio_motor;
                $MotorAverage = ((round(($price*100)/$average_motor))-100);
                $MotorAverageTooltip = '$'.number_format ( $average->promedio_motor, 0 , "," , "." ).' ('.$average->qty_ml.')';
            } // SOLO SI PROMEDIO MOTOR NO ESTE VACIO


            $Ranges=Range::all()->where('average_id',$average['id']);
            $rangekm=0;
            $rangeqty=0;
            $rangemin=0;
            $rangemax=0;
            foreach ($Ranges as $Range) {
                if($r->KILOMETERS > $Range->min && $r->KILOMETERS < $Range->max){
                    $rangekm=$Range->average;
                    $rangeqty=$Range->qty;
                    $rangemin=round(($Range->min/1000),0);
                    $rangemax=round(($Range->max/1000),0);
                }
            } // buscando el rango de km adecuado 

            if ($rangekm!=0) {
                $averagekm=$rangekm;
                $MlAverageKM = ((round(($price*100)/$averagekm))-100);
                $MlAverageKMTooltip = '<em>'.$rangemin.'milKM</em> ~ <em>'.$rangemax.'milKM</em> <br>$'.number_format ( $rangekm, 2 , "," , "." ).' ('.$rangeqty.')';
            } // MlAverageKM


            $month = date('m');
            $currentyear = date('Y');
            $CurrentKM = (14000/12)*$month;
            $Age = (float)$currentyear-(float)($r->VEHICLE_YEAR);
            $AgeKM = $Age*14000;
            $difAge = $Age-((float)$currentyear-2007);
            $difAgeKM=0;
            if($difAge>0){
                $difAgeKM=$difAge*6000;
            }
            $ExpectedKM=$AgeKM+$difAgeKM+$CurrentKM;
            $ExpectedKM=round($ExpectedKM,0);
            $AverageExpectedKM = ((round(($r->realkm*100)/$ExpectedKM))-100);

            $AverageExpectedKMTooltip = '<b>Tiene:</b> '.round(($r->realkm/1000),1).'milKM <br> <b>De:</b> '.round(($ExpectedKM/1000),1).'milKM '; 
        }// QUE EXISTA UN AVERAGE 

        $History = History::create([
            'publication_id' => $r->id,
            'ml_average' => $MlAverage,
            'ml_average_tooltip' => $MlAverageTooltip,
            'ml_average_km' => $MlAverageKM,
            'ml_average_km_tooltip' => $MlAverageKMTooltip,
            'motor_average' => $MotorAverage,
            'motor_average_tooltip' => $MotorAverageTooltip,
            'expected_km' => $ExpectedKM,
            'average_expected_km' => $AverageExpectedKM,
            'average_expected_km_tooltip' => $AverageExpectedKMTooltip,
            'price' => $price
        ]);        }

    return '';
}

function createAverage($Publications,$promedio_motor,$model,$year,$version,$RealVersion){
    $sum=0;
    $cont=0;
    $minmaxs = array(
    array(0,10000),
    array(10001,20000),
    array(20001,30000),
    array(30001,40000),
    array(40001,50000),
    array(50001,60000),
    array(60001,70000),
    array(70001,80000),
    array(80001,90000),
    array(90001,100000),
    array(100001,200000)
    );
    $sumkm = array(0,0,0,0,0,0,0,0,0,0,0); //DEBE TENER EL MISMO TAMANO DE MINMAXS
    $contkm = array(0,0,0,0,0,0,0,0,0,0,0); //DEBE TENER EL MISMO TAMANO DE MINMAXS
    $allranges = array(0,0,0,0,0,0,0,0,0,0,0); //DEBE TENER EL MISMO TAMANO DE MINMAXS
    
    foreach ($Publications as $r) {
        if ($r->VEHICLE_YEAR==$year) {
                $sum += $r->price;
                $cont += 1;
                $i=0;
                foreach ($minmaxs as $minmax) {
                    if($r->KILOMETERS>$minmax[0] && $r->KILOMETERS<$minmax[1]){
                        $sumkm[$i] += $r->price;
                        $contkm[$i] += 1;
                        $allranges[$i] = array($minmax[0],$minmax[1],$sumkm[$i],$contkm[$i]);
                    }
                    $i++;
                }
        }
    }

    if ($cont!=0) {
        $promedio_ml= $sum/$cont;
    }else{
        $promedio_ml=0;
    }

    $Average = Average::create([
        'promedio_ml' => $promedio_ml,
        'promedio_motor' => $promedio_motor,
        'model_id' => $model,
        'year' => $year,
        'version_id' => $version,
        'qty_ml' => $cont,
        'trim' => $RealVersion,
    ]);

    foreach ($allranges as $range) {
        if($range[2]!=0 && $range[3]!=0){
            $averagekm = $range[2]/$range[3];
            Range::create([
            'average_id' => $Average->id,
            'min' => $range[0],
            'max' => $range[1],
            'average' => $averagekm,
            'qty' => $range[3]
             ]);
        }
    }

    return '';
}//CREATEAVERAGE