<?php
use Symfony\Component\HttpFoundation\Response;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
*/

Route::view('/', 'landing');

Auth::routes();

Route::group(['middleware' => ['auth']], function () { //MIDDLEWARE
Route::get('home', 'HomeController@index');
Route::prefix('dash')->group(function () {
 	Route::get('/', 'HomeController@index')->name('dash'); 
 	Route::get('search', 'PublicationController@search')->name('search');
 	Route::get('brands/{bran_id?}', 'HomeController@viewAll')->name('brands');
 	Route::get('models/{model_id}', 'HomeController@viewModel')->name('models');

	Route::resource('publications', 'PublicationController'); 
	Route::get('publication/newcreate', 'PublicationController@newCreate'); 
	Route::get('publication/{publication_id}/deleting', 'PublicationController@destroy');
	Route::get('publication/{publication_id}/sold', 'PublicationController@sold'); 
	Route::get('publication/showversions', 'PublicationController@showVersions'); 
	Route::get('publication/byversion/{version_id}', 'PublicationController@publicationsByVersion');

	Route::post('searchpublications', 'PublicationController@searchPublications');   
	Route::resource('averages', 'AverageController');
	Route::resource('versions', 'VersionController');  
	Route::get('version/view/{bran_id?}', 'VersionController@viewAll');  
	Route::resource('subversions', 'SubversionController');  
	Route::get('versions/model/{model}', 'VersionController@searchByModel'); 
	Route::get('versions/{version}/model/{model}/subversions', 'VersionController@searchSubversions'); 
	Route::get('versions/model/{model}/subversions/hiddingtakens', 'VersionController@searchSubversionsHT'); 
	Route::get('versions/{version_id}/publications', 'AverageController@tagPublications');  
	Route::post('versions/subversion/create', 'SubversionController@storeAjax');
	Route::get('versions/{version}/subversion/{name}/delete', 'SubversionController@deleteAjax');
	Route::get('models/brand/{brand}', 'PublicationController@searchByBrand');
	Route::post('publications/search', 'PublicationController@results');
	Route::get('publications/more/attr/{id_model}/{version_id}', 'VersionController@getMoreattr');



	//HELPERS AJAX
	Route::get('helper/get_versions/{model}', 'VersionController@getVersionsByModel');
	Route::get('helper/has_pub/{cat}', function($cat) { return hasPub($cat); });
	Route::get('helper/get_all_pub/{brand}/{model}/{ajax?}', function($brand,$model,$ajax='true') {  
		$publications = getAllPub($model); 
		if ($ajax == 'true') {  
			if ($publications) {
				$q=storeUpdatePublications($brand,$publications);
				return response()->json(['status'=>true,'data'=>$q]);
			}else{
				return response()->json(['status'=>false]);
			} 
		}else{ 
			dd(($publications)? storeUpdatePublications($brand,$publications):$publications);
		} 
	});
	
	
    //USERS
	Route::resource('users', 'UserController');
});//END DASH
});//END MIDDLEWARE