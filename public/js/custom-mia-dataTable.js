var initGeneralEvents=false;
function eventsAllTables() { 
	if (!initGeneralEvents) {
	    //EVENTO SCROLL (SHOW/HIDE HEAD FIX)
		var lastScrollTop = 0; 
		$(window).on('scroll', function() { 
		    st = $(this).scrollTop();  
	    	if (st < lastScrollTop){  
	    		up=true;  
	    	}else{ 
	    		$('.yadcf-filter-wrapper.show').each(function(e){$(this).removeClass('show')}); 
	    		up=false;  
	    	} 
		    lastScrollTop = st;
		});

		//EVENTO SORTING DE COLUMNAS CON FECHA 
		function sortByDate(x,y) {
	        var regex = /(3[01]|[12][0-9]|0?[1-9])[- /.](1[012]|0?[1-9])[- /.]((?:18|20)\d{2})/gi;
	        //si no tiene fecha pongo una vieja 
	        (x.match(regex)==null)? x='01/01/1891':null;
	        (y.match(regex)==null)? y='01/01/1891':null; 
	        //uso regex por si el campo viene envuelto en algun tag html 
	        var x1 = x.match(regex)[0].split('/'); 
	        var y1 = y.match(regex)[0].split('/');
	        // paso a milisegundos para comprar
	        var x2 = new Date(x1[2], x1[1], x1[0]).getTime();
	        var y2 = new Date(y1[2], y1[1], y1[0]).getTime();
	        return {'x':x2,'y':y2}; 
		}
	    jQuery.fn.dataTableExt.oSort['date-case-asc']  = function (x,y) { 
	    	if (x && y) {
	    		var date = sortByDate(x,y);
		        return ((date.x < date.y) ? -1 : ((date.x > date.y) ?  1 : 0)); 
		    }
	    };  
	    jQuery.fn.dataTableExt.oSort['date-case-desc'] = function (x,y) { 
	    	if (x && y) {
		        var date = sortByDate(x,y);
		        return ((date.x < date.y) ?  1 : ((date.x > date.y) ? -1 : 0));
		    }
	    }; 
		initGeneralEvents = true;
	}
}

//SMART INIT
function smartInitDateTable() { 
	var oTables = [];
	$('.init-dataTable').each(function (key,table) {
		//recojo info de las cabeceras
		var aoColumns = [];
		var infoColums = [];
		$(table).find('th').each(function (key, head) {   
			infoColums.push({
				'key': key,
				'filter-type': $(head).attr('filter-type')
			});
			($(head).attr('filter-type') == 'range_date')? aoColumns.push({ "sType": 'date-case' }) : aoColumns.push(null);
		});

		//preparo datos para iniciar dataTable y yadcf
		var filters = [];
		$(infoColums).each(function (key,colum) { 
			//Pluigin yadcf
			switch(colum['filter-type']) {
				case "range_date": 
					filters.push({
						column_number: colum['key'], 
						filter_type: "range_date",  
						datepicker_type: 'bootstrap-datepicker', 
						date_format: 'dd/mm/yyyy', 
						moment_date_format: 'DD/MM/YYYY', 
						column_data_type:'html', 
						filter_default_label: ["Desde", "Hasta"],
						filter_reset_button_text: "Borrar", 
					});
					break;
				case "multi_select":
					filters.push({
						column_number: colum['key'],
						filter_type: "multi_select",
						select_type: 'select2',
						filter_reset_button_text: "Borrar",
						column_data_type:'html',
						filter_match_mode: 'exact',
						select_type_options: { 
							placeholder: 'Selecciona para filtrar', 
						},
					});
					break;
				case "text":  
					filters.push({
						column_number: colum['key'],
						filter_type: "text",
						text_data_delimiter: ",",
						style_class: 'form-control',
						filter_default_label: 'Escribe para filtrar',
						filter_reset_button_text: "Borrar", 
					});
					break;
				case "range_number_slider":
					filters.push({
						column_number: colum['key'],
						filter_type: "range_number_slider",
						ignore_char: '[^0-9-]',
						filter_reset_button_text: "Borrar",
					});
					break;
				case "range_number":
					filters.push({
						column_number: colum['key'],
						filter_type: "range_number",
						filter_default_label: ["Desde", "Hasta"],
						// ignore_char: '[^0-9-]',
						filter_reset_button_text: "Borrar",
					});
					break;
				default:
					break;
			}  
		}); 

		var oTable = $(table).DataTable({
		    // "bStateSave": true,
		    'dom': 'Blfrtip',
		    "bJQueryUI": true,
		    'colReorder': true,
		    'keys': true,
		    "pageLength": 100,
		    "responsive": true,
		    'searchHighlight': true,
		    "bAutoWidth": false,
		    "language": {
		        "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json",
		        'searchPlaceholder': "Buscar registros",
		    },
		    'buttons': [
		        {
		            'extend': 'excelHtml5',
		            'exportOptions': {
		                'format': {
		                    header: function(text,index,node){ return $(node).find('.t-head').text(); }
		                },
		                'modifier' : { 'selected': null },
		            }
		        },{
		            'extend': 'pdfHtml5',
		            'exportOptions': {
		                'format': {
		                    header: function(text,index,node){ return $(node).find('.t-head').text(); }
		                },
		                'modifier' : { 'selected': null },
		            }
		        }
		    ],
		    'select': {
		        'toggleable': true,
		        'blurable': true
		    },
		    // 'fixedHeader': {
		    //     'header': true,
		    //     'headerOffset': $('#head-side-content').height()
		    // }, 
		    "columnDefs": [
		        { "orderable": false, "targets": '_all' }, 
		    ],
		    "aoColumns": aoColumns
		}); 
		if (filters.length) {yadcf.init(oTable,filters);}
	    initEventsDateTable(oTable);
	    oTables.push(oTable);
	});
	eventsAllTables();
	return oTables;
}

//MANUAL INIT
function initEventsDateTable(oTable){ 
    oTable.on('draw', function () {
    	var table_id = oTable.table().node().id;
        if (!$('#'+table_id+' th[num-col-head]').length) {  

            // ADD BOTON RESET 
            $('#'+table_id+'_wrapper').prepend('<a href="#" class="btn btn-sm btn-outline-success resett" data-table-id="'+table_id+'" style="float:right; margin-left: 15px;">Limpiar</a>');
		    
		    //EVENTO RESRET
		    $('#'+table_id+'_wrapper .resett').click(function(e){
		    	e.preventDefault();
		    	yadcf.exResetAllFilters(oTable); 
		    });

		 	//ADD WAPPER OPTIONS
		 	$('#'+table_id+'_wrapper').prepend(
		 		'<div class="block-header options-filter pb-5 pl-0 pr-0" >'+
	                '<div  class="btn btn-sm btn-outline-dark wrapp-options-filter">'+
	                    '<i class="fas fa-times fa-lg close-icon toggle-filter"></i>'+
	                    '<i class="fas fa-sliders-h filter-icon toggle-filter"></i>'+
	                    '<div class="clearfix"></div>'+
					'</div>'+
            	'</div>'
            );
		    
		    //ADD OPCIONES HIDE/SHOW - EXPORT - CUSTOM OPTIONS 
		    $('#'+table_id+'_wrapper .wrapp-options-filter').append($('#'+table_id+'_wrapper').siblings('.block-options-dataTable')); 

		    $('#'+table_id+'_wrapper .wrapp-options-filter').append(
		    	'<div class="options-hide-columns">'+
		    		'<div class="tittle-option">Ocultar/Mostrar columnas</div>'+
		    		'<div class="buttons-option"></div>'+
		    	'</div>'
		    ); 
		    $('#'+table_id+'_wrapper .wrapp-options-filter').append(
		    	'<div class="options-export">'+
		    		'<div class="tittle-option">Exportar tabla</div>'+
		    		'<div class="buttons-option">'+
		    			'<button class="btn btn-sm btn-outline-success export-pdf"> PDF </button>'+
		    			'<button class="btn btn-sm btn-outline-success export-excel"> EXCEL </button>'+
		    		'</div>'+
		   		'</div>'
		   	);

		    //EVENTO EXPORT  
		    $('#'+table_id+'_wrapper .export-excel').click(function(){
		        $('#'+table_id+'_wrapper .buttons-excel').click();
		    }); 
		    $('#'+table_id+'_wrapper .export-pdf').click(function(){
		        $('#'+table_id+'_wrapper .buttons-pdf').click();
		    });

		    //EVENTO TOGGLE OPEN DIV OPCIONES 
		    $('#'+table_id+'_wrapper i.toggle-filter').click(function(){ 
		        $('#'+table_id+'_wrapper .wrapp-options-filter').toggleClass('open-filter');  
		    });   

		    //FILTRADO PARA VISTAS
		    $('#'+table_id+'_wrapper [custom-filter]').on('click',function(){
		        var filters = $(this).attr('custom-filter').match(/\[.*?\]/g);   
		        var columns = $(this).attr('custom-filter-column').split("-");
		        var sort = $(this).attr('custom-sort');
		        var sortColumn = $(this).attr('custom-sort-column'); 
		        if (columns != undefined && columns !=''  && columns.length == filters.length) { 
		            $(filters).each(function(index, value) { 
		                    value = value.replace("[","").replace("]","");
		                    oTable.column(columns[index]).search(value,true,false); 
		            }); 
		            oTable.draw(); 
		            if (sort != undefined && sort !=''  && sortColumn != undefined && sortColumn !='') { 
		                oTable.order([sortColumn,sort]).draw();
		            }else{
		                console.log('No se especifico la columna o el sortby');
		            }      
		        }else{
		            console.log('Filtro multi columna mal ejecutado. Seguir el Ej: custom-filter="[word|planet][23|40]" custom-filter-column="7-2"');
		        }
		    }); 
 
            //EMNUMERAR CABECERA,CREAR EVENTOS DE CABECERA (SORTING Y MOSTRAR POPUP FILTRO) Y AGREGAR BOTONOS HIDE COLUM
            $('#'+table_id+'>thead>tr th').each(function(numCol) {
                $(this).attr("num-col-head", numCol);
                $(this).removeClass('sorting_disabled').addClass('sorting');
                ($(this)[0].hasAttribute('filter-type'))? $(this).append('<i class="icon-search-column fa fa-search"></i>'):null;
                $(this).on('click', function(e){
                    if (!$(e.target).hasClass('icon-search-column')) {
                        ($(this).hasClass('sorting_desc'))? oTable.order([[numCol,'asc']]).draw() : oTable.order([numCol,'desc']).draw();
                    }else{  
                        $('#yadcf-filter-wrapper--'+table_id+'-'+numCol).toggleClass('show'); 
                    }
                });   
                $('#'+table_id+'_wrapper  div.options-hide-columns div.buttons-option').append(
                	'<button class="btn btn-sm btn-outline-success toggle-eye" data-column="'+numCol+'">'+
                		$(this).find('span.title-head').text()+
                		'<span class="fa fa-fw fa-eye field-icon"></span>'+
                	' </button>'
                );
            });

		    //EVENTO HIDE-SHOW COLUMNA
		    $('#'+table_id+'_wrapper .toggle-eye').click(function(e) {
		        e.preventDefault();
		        $(this).toggleClass('btn-outline-success');
		        $(this).toggleClass('btn-outline-secondary');
		        $(this).find("span").toggleClass("fa-eye fa-eye-slash");  
		        var column = oTable.column($(this).attr('data-column')); 
		        column.visible(!column.visible());
		    }); 

            //AGREGO ESTILOS
            $('#'+table_id+'_wrapper input').addClass('form-control'); 
        } 
		//RESALTA LOS RESULTADOS
        var body = $(oTable.table().body());
        body.unhighlight();
        body.highlight(oTable.search());  
    }); 
    eventsAllTables();
}

//Smooth scrolling
$(document).on('click','.paginate_button',function(){
	var id = '#'+$(this).attr('aria-controls')+'_wrapper'; 
	$('html, body').animate({
		scrollTop: $(id).offset().top
	});
});