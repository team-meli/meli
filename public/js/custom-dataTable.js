function initCustomDateTable(oTable){
    //ENVENT RESRET
    $(document).on('click','.resett',function(e){
        yadcf.exResetAllFilters(oTable);
    });

    //ADD ICON FILTER
    $(".search").each(function(index) {
        $(this).append('<i style="" class="icon-search nav-main-link-icon fa fa-search"></i>');
    });
    
    //EMNUMERAR CABECERA
    $("tr").first().children().each(function(index) {
        $(this).attr("num-col-head", index);
    });

    //EMNUMERAR CABECERA FALSA
    $(".wrap-search").children().each(function(index) {
        $(this).attr("num-col", index);
    });        

    //EVENTO PARA OCULTAR INPUT DE FILTRO
    $(document).on("click", function(event){
        var tar=$(event.target)[0];
        if(!$(tar).hasClass('yadcf-filter-wrapper') && !$(tar).hasClass('icon-search') && !$(tar).hasClass('no-close')  && !$(tar).hasClass('ui-datepicker-title') && !$(tar).hasClass('ui-datepicker-header') && !$(tar).hasClass('ui-datepicker-other-month') && !$(tar).hasClass('ui-icon') && !$(tar).hasClass('ui-datepicker-prev') && !$(tar).hasClass('ui-datepicker-next') ){
            $( ".yadcf-filter-wrapper" ).each(function( index ) {
                hidefilter();
            });
        }else{
            if ($(tar).hasClass('icon-search')) {
                var numFiltro = $(tar).parent().attr('num-col');
                var wrapFilter = $('#yadcf-filter-wrapper--example-'+numFiltro);
                if (!$(wrapFilter).hasClass('ocultame')) {
                    $(wrapFilter).css('display','inherit');
                    $(wrapFilter).addClass('ocultame');
                }else{

                    $(wrapFilter).css('display','none');
                    $(wrapFilter).removeClass('ocultame');
                }              
            }
        }      
    });

    //FUNCION QUE OCULTA LOS FILTROS
    function hidefilter(){
        $( ".yadcf-filter-wrapper" ).each(function( index ) {
            ($(this).hasClass('ocultame'))? $(this).removeClass('ocultame') :null ;
            $(this).css('display','none');
        });
    }    
    //EDIT TABLA DESPUES DE DIBUJADA
    $(document).on('draw.dt', function () {

        if ($('a.resett').length == 0) {

            // ADD BOTON RESET 
            $('#example_wrapper').prepend('<a href="#" class="btn btn-sm btn-outline-success resett resett-date" style="float:right; margin-left: 15px;">Limpiar</a>');

            //AGREGO ESTILOS
            $('#example_length>label>select').addClass('form-control form-control-sm');
            $( "input" ).each(function( index ) {
                $(this).addClass('form-control form-control-sm');
            });

            //CAMBIO LOS PLACEHOLDER
            $( ".yadcf-filter" ).each(function( index ) {
                $(this).attr("placeholder", "Escribe para filtrar");
            });

        }
    });

    // SyntaxHighlighter.all();
}
