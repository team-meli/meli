$(function() {  
  $('.js-datepicker').datepicker({
    format: 'yyyy/mm/dd',
    startDate: '-3d'
  }); 

  $(".delete_button").click(function(event){
    event.preventDefault();
    if (confirm("¿Estás seguro de borrar esto?")){
      $(this).parent().submit();
    }
  }); 

  $('body').tooltip({selector: '[data-toggle="tooltip"]'});

  $('.js-select2').select2();
});

//HELPERS
function getBrands() {
  $.ajax({
    url: "https://api.mercadolibre.com/categories/MCO1744",
    // beforeSend: function(xhr) { 
    //   xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password")); 
    // },
    type: 'GET',
    dataType: 'json',
    contentType: 'application/json', 
    success: function (data) {
      var options='';
      $(data.children_categories).each(function(key,brand) {
        var option = '<option value="'+brand.id+'">'+brand.name+' ('+brand.total_items_in_this_category+') - '+brand.id+'</option>';
        options = options+' '+option; 
      });
      $('#brand').append(options);
    }
  });
} 

function getModels(brand) {
  $.ajax({
    url: "https://api.mercadolibre.com/categories/"+brand,
    // beforeSend: function(xhr) { 
    //   xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password")); 
    // },
    type: 'GET',
    dataType: 'json',
    contentType: 'application/json', 
    success: function (data) {
      var options='<option selected disabled >Selecciona un modelo</option>';
      $(data.children_categories).each(function(key,brand) {
        var option = '<option ml-name="'+brand.name+'" value="'+brand.id+'">'+brand.name+' ('+brand.total_items_in_this_category+') - '+brand.id+'</option>';
        options = options+' '+option; 
      });
      $('#model').attr('disabled',false).html(options);
    }
  });
}

function getName(cat) {
  var name;
  $.ajax({
    url: "https://api.mercadolibre.com/categories/"+cat,
    // beforeSend: function(xhr) { 
    //   xhr.setRequestHeader("Authorization", "Basic " + btoa("username:password")); 
    // },
    type: 'GET',
    async: false,
    dataType: 'json',
    contentType: 'application/json', 
    success: function (data) {
      name = data.name;
    }
  });
  return name;
}

function hasPub(cat) { 
  var result; 
  $.ajaxSetup({
    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
  })
  $.ajax({ 
    url: "/dash/helper/has_pub/"+cat,
    method: 'GET',
    async: false,
    data: {cat: cat},
    success: function (data) { 
      result = JSON.parse(data);
    }
  }); 
  return result;
}
 
function getAllPub(brand,model) { 
  var result;
  console.log('/dash/helper/get_all_pub/'+brand+'/'+model);
  $.ajaxSetup({
    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
  });
  $.ajax({ 
    url: '/dash/helper/get_all_pub/'+brand+'/'+model,
    method: 'GET', 
    async: false,
    success: function (data) {
      result = data;
    },
    error: function (error) {
      console.log(error);
    }
  }); 
  return result;
} 