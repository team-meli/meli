function initRangeDatePicker(oTable,col){

    //AGREGAR DIV PARA FILTRAR FECHA
    $.each(col, function( index, value ) {
        $('body').append('  <div class="hide date_filter" move-to-col='+value+'>'+
                                '<div>'+
                                   '<input placeholder="Desde" class="datepicker_from date_range_filter date " type="text" /> '+
                                '</div>'+
                                '<div >'+
                                   '<input placeholder="Hasta" class="datepicker_to date_range_filter date " type="text"  />'+ 
                                '</div>'+
                                '<button type="button" class="yadcf-filter-reset-button range-number-slider-reset-button resett" style="width: 100%;">Borrar</button>'+
                                '<div class="custom-control custom-checkbox no-close">'+
                                    '<input type="checkbox" class="custom-control-input checkFilter no-close" id="customCheck2">'+
                                    '<label class="custom-control-label no-close" for="customCheck2">Incluir no iniciadas</label>'+
                                '</div>'+
                            '</div>');
    });

    //FECHA MINIMA 
    $(".datepicker_from").datepicker({
        dateFormat: 'dd/mm/yy',
        showOn: "button",
        buttonImage: "/assets-landing/img/calander.png",
        buttonImageOnly: false,
        "onSelect": function(date) {
            $('.datepicker_from.activeFilter').removeClass('activeFilter');
            $(this).addClass('activeFilter');
        }
    }).keyup(function() {
        var dateArr = this.value.split('/');
        var date = dateArr[1]+'/'+dateArr[0]+'/'+dateArr[2];
        minDateFilter = new Date(date).getTime();
    });

    //FECHA MAXIMA
    $(".datepicker_to").datepicker({
        dateFormat: 'dd/mm/yy',
        showOn: "button",
        buttonImage: "/assets-landing/img/calander.png",
        buttonImageOnly: false,
        "onSelect": function(date) {
            $('.datepicker_to.activeFilter').removeClass('activeFilter');
            $(this).addClass('activeFilter');
            oTable.fnDraw();
        }
    }).keyup(function() {
        var dateArr = this.value.split('/');
        var date = dateArr[1]+'/'+dateArr[0]+'/'+dateArr[2];
        maxDateFilter = new Date(date).getTime();
    });
    

    //ASIGA NUMERO DE CABECERA A LA QUE PERTENECE IPUT FILTRO FECHAS
    $(".datepicker_from, .datepicker_to , .checkFilter").each(function(index) {
        var whatCol=$(this).parents(':eq(1)')[0]; 
        var col = $(whatCol).attr("move-to-col");
        $(this).attr("move-to-col", col);      
    });


    //EDIT TABLA DESPUES DE DIBUJADA (Agrega los divs POP UP & mueve elementos para el filtrado de fechas )
    $(document).on('draw.dt', function () {

        if (Array.isArray(col)) {
            ($('#yadcf-filter-wrapper--example-'+col[0]).length == 0 )? col.forEach(addInputFilter) : null;
        }else{
            ($('#yadcf-filter-wrapper--example-'+col).length == 0 )? addInputFilter(col) : null; 
        }
    });

    //ENVENT RESRET
    $(document).on('click','.resett',function(e){
        e.preventDefault();
        $('.datepicker_from').addClass('resetFilterDate');
        oTable.fnDraw();
        $('.datepicker_from , .datepicker_to').val('');
        $(".checkFilter").prop('checked', false);
        $('.resetFilterDate').removeClass('resetFilterDate');
    });

    function formatDate(date){
        var dateArr = date.split('/');
        var date = dateArr[1]+'/'+dateArr[0]+'/'+dateArr[2];
        var fecha = new Date(date).getTime();
        return fecha;
    }

    function addInputFilter(colum){
        var divwrap = '<div id="yadcf-filter-wrapper--example-'+colum+'" class="yadcf-filter-wrapper wrapper-datarange-custom" ></div>';
        $( "th[num-col-head='"+colum+"']" ).append(divwrap);
        $('#yadcf-filter-wrapper--example-'+colum).append($(".date_filter[move-to-col='"+colum+"']").removeClass('hide') );
    }

    $.fn.dataTableExt.afnFiltering.push(
        function(oSettings, aData, iDataIndex){

            if ($(".resetFilterDate").length) { return true;}
            if ($('.datepicker_from.activeFilter').val() === undefined && $('.datepicker_to.activeFilter').val() === undefined) { return true; }

            columna = $('.datepicker_from.activeFilter').attr("move-to-col");
            minDateFilter = formatDate($('.datepicker_from.activeFilter').val());
            maxDateFilter = formatDate($(' .datepicker_to.activeFilter ').val());                   

            if(aData[columna]!='No iniciada'){ 
                var adataDate = aData[columna];
                if (adataDate.search("/")!=-1) {
                    aData._date = formatDate(aData[columna]); 
                }else{
                    aData._date = formatDate('01/01/'+aData[columna]); 
                }
            }else{ 
                if ($(".checkFilter[move-to-col='"+columna+"']").length && $(".checkFilter[move-to-col='"+columna+"']").is(":checked")) {
                    return true; 
                }else{
                    return false; 
                }
            }

            //OPERACIONES
            if (minDateFilter =='' || maxDateFilter =='' ||  isNaN(minDateFilter) ||  isNaN(maxDateFilter)) { return true; }
            if (minDateFilter && !isNaN(minDateFilter) && aData._date < minDateFilter) { return false; }
            if (maxDateFilter && !isNaN(maxDateFilter) && aData._date > maxDateFilter) { return false; }

            return true;
        }
    );        
}
